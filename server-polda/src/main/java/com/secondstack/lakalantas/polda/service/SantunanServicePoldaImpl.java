package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.Santunan;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.service.impl.SantunanServiceImpl;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("santunanPoldaService")
public class SantunanServicePoldaImpl extends SantunanServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @Override
    public List<Santunan> find() {
        List<Polres> polreses = polresService.find();
        List<Santunan> santunans = new ArrayList<Santunan>();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "santunan").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<Santunan[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Santunan[].class);
            santunans.addAll(Arrays.asList(response.getBody()));
        }

        return santunans;
    }

    @Override
    public Santunan find(String id) {
        List<Polres> polreses = polresService.find();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "santunan?id=" + id).replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            try {
                ResponseEntity<Santunan> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Santunan.class);
                if(response.getBody() != null){
                    return response.getBody();
                }
            } catch (RestClientException e) {
                e.printStackTrace();
                continue;
            }
        }

        return null;
    }

}
