package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.Asuransi;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.domain.RumahSakit;
import com.secondstack.lakalantas.core.service.AsuransiService;
import com.secondstack.lakalantas.core.service.RumahSakitService;
import com.secondstack.lakalantas.core.service.impl.PolresServiceImpl;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("polresPoldaService")
public class PolresServicePoldaImpl extends PolresServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("asuransiPoldaService")
    private AsuransiService asuransiService;

    @Autowired
    @Qualifier("rumahSakitPoldaService")
    private RumahSakitService rumahSakitService;

    @Override
    public void save(Polres polres) {

        super.save(polres);

        List<Polres> polreses = find();

        polreses.add(polres);
        try {
            String url = (SystemUtils.HOST_WS_POLRES + "polres/list").replace("host", polres.getHost()).replace("port", polres.getPort() + "");
            HttpEntity<List<Polres>> httpEntity = new HttpEntity<List<Polres>>(polreses);
            restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);

            String urlAsuransi = (SystemUtils.HOST_WS_POLRES + "asuransi/list").replace("host", polres.getHost()).replace("port", polres.getPort() + "");
            HttpEntity<List<Asuransi>> httpEntityAsuransi = new HttpEntity<List<Asuransi>>(asuransiService.find());
            restTemplate.exchange(urlAsuransi, HttpMethod.POST, httpEntityAsuransi, String.class);

            String urlRumahSakit = (SystemUtils.HOST_WS_POLRES + "rumahSakit/list").replace("host", polres.getHost()).replace("port", polres.getPort() + "");
            HttpEntity<List<RumahSakit>> httpEntityRumahSakit = new HttpEntity<List<RumahSakit>>(rumahSakitService.find());
            restTemplate.exchange(urlRumahSakit, HttpMethod.POST, httpEntityRumahSakit, String.class);

        } catch (RestClientException e) {
            e.printStackTrace();
            return;
        }
        polreses.remove(polres);


        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "polres").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity<Polres> httpEntity = new HttpEntity<Polres>(polres);
            restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        }

    }

    @Override
    public void update(Polres polres) {
        super.update(polres);

        List<Polres> polreses = find();
        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "polres").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity<List<Polres>> httpEntity = new HttpEntity<List<Polres>>(polreses);
            restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
        }
    }

    @Override
    public void delete(Polres polres) {
        super.delete(polres);

        List<Polres> polreses = find();
        Map uriVar = new HashMap();
        uriVar.put("id", polres.getId());

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "polres?id={id}").replace("host", p.getHost()).replace("port", p.getPort() + "");
            restTemplate.exchange(url, HttpMethod.DELETE, null, String.class, uriVar);
        }
    }
}
