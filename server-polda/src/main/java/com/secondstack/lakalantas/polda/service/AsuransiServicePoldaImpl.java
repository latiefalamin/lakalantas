package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.Asuransi;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.service.impl.AsuransiServiceImpl;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("asuransiPoldaService")
public class AsuransiServicePoldaImpl extends AsuransiServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @Override
    public void save(Asuransi asuransi) {
        List<Polres> polreses = polresService.find();

        for(Polres p:polreses){
            String url = (SystemUtils.HOST_WS_POLRES + "asuransi").replace("host", p.getHost()).replace("port",p.getPort() + "");
            HttpEntity<Asuransi> httpEntity = new HttpEntity<Asuransi>(asuransi);
            restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        }
        super.save(asuransi);
    }

    @Override
    public void delete(Asuransi asuransi) {
        super.delete(asuransi);

        List<Polres> polreses = polresService.find();
        List<Asuransi> asuransies = find();
        Map uriVar = new HashMap();
        uriVar.put("id", asuransi.getId());

        for(Polres p:polreses){
            String url = (SystemUtils.HOST_WS_POLRES + "asuransi?id={id}").replace("host", p.getHost()).replace("port",p.getPort() + "");
            restTemplate.exchange(url, HttpMethod.DELETE, null, String.class, uriVar);
        }
    }
}
