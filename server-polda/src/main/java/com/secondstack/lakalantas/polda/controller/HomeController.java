package com.secondstack.lakalantas.polda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Author : Latief Al Amin
 * allatief04032@gmail.com
 *
 * @latief
 */
@Controller
@RequestMapping(value = "/home")
public class HomeController {

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap modelMap) {
        return "redirect:/home/dashboard";
    }

    @RequestMapping(value = "dashboard", method = RequestMethod.GET)
    public String dashboard(ModelMap modelMap){
        return "dashboard-page";
    }

}
