package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.Korban;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.service.impl.KorbanServiceImpl;
import com.secondstack.lakalantas.core.simple.SimpleKorban;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("korbanPoldaService")
public class KorbanServicePoldaImpl extends KorbanServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @Override
    public List<Korban> find() {
        List<Polres> polreses = polresService.find();
        List<Korban> korbans = new ArrayList<Korban>();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "korban").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<Korban[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Korban[].class);
            korbans.addAll(Arrays.asList(response.getBody()));
        }

        return korbans;
    }

    @Override
    public List<SimpleKorban> findSimple() {
        List<Polres> polreses = polresService.find();
        List<SimpleKorban> korbans = new ArrayList<SimpleKorban>();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "korban?isSimple=true").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<SimpleKorban[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, SimpleKorban[].class);
            korbans.addAll(Arrays.asList(response.getBody()));
        }

        return korbans;
    }

    @Override
    public Korban find(String id) {
        List<Polres> polreses = polresService.find();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "korban?id=" + id).replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            try {
                ResponseEntity<Korban> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Korban.class);
                if(response.getBody() != null){
                    return response.getBody();
                }
            } catch (RestClientException e) {
                e.printStackTrace();
                continue;
            }
        }

        return null;
    }

}
