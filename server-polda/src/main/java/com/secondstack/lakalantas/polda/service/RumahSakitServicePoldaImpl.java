package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.RumahSakit;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.service.impl.RumahSakitServiceImpl;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("rumahSakitPoldaService")
public class RumahSakitServicePoldaImpl extends RumahSakitServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @Override
    public void save(RumahSakit rumahSakit) {
        super.save(rumahSakit);

        List<Polres> polreses = polresService.find();

        for(Polres p:polreses){
            String url = (SystemUtils.HOST_WS_POLRES + "rumahSakit").replace("host", p.getHost()).replace("port",p.getPort() + "");
            HttpEntity<RumahSakit> httpEntity = new HttpEntity<RumahSakit>(rumahSakit);
            restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        }
    }

    @Override
    public void delete(RumahSakit rumahSakit) {
        super.delete(rumahSakit);

        List<Polres> polreses = polresService.find();
        List<RumahSakit> rumahSakites = find();
        Map uriVar = new HashMap();
        uriVar.put("id", rumahSakit.getId());

        for(Polres p:polreses){
            String url = (SystemUtils.HOST_WS_POLRES + "rumahSakit?id={id}").replace("host", p.getHost()).replace("port",p.getPort() + "");
            restTemplate.exchange(url, HttpMethod.DELETE, null, String.class, uriVar);
        }
    }
}
