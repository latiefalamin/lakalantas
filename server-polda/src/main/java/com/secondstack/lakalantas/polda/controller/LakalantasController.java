package com.secondstack.lakalantas.polda.controller;

import com.secondstack.lakalantas.core.domain.Lakalantas;
import com.secondstack.lakalantas.core.domain.enumeration.JenisLakalantas;
import com.secondstack.lakalantas.core.domain.enumeration.KondisiKendaraan;
import com.secondstack.lakalantas.core.editor.LakalantasEditor;
import com.secondstack.lakalantas.core.service.KendaraanService;
import com.secondstack.lakalantas.core.service.LakalantasService;
import com.secondstack.lakalantas.core.service.PenyidikService;
import com.secondstack.lakalantas.core.simple.SimpleLakalantas;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "lakalantas")
public class LakalantasController {

    @Autowired
    @Qualifier("lakalantasPoldaService")
    private LakalantasService lakalantasService;

    @Autowired
    @Qualifier("penyidikPoldaService")
    private PenyidikService penyidikService;

    @Autowired
    @Qualifier("kendaraanPoldaService")
    private KendaraanService kendaraanService;

    @RequestMapping(value = "navbar", method = RequestMethod.GET)
    public String navBar() {
        return "lakalantas/navbar";
    }

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("lakalantass", lakalantasService.findSimple());
        return "lakalantas/list";
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("id") Lakalantas lakalantas) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("jenisLakalantases", JenisLakalantas.values());
        modelMap.addAttribute("kondisiKendaraans", KondisiKendaraan.values());
        modelMap.addAttribute("penyidiks", penyidikService.find());
        modelMap.addAttribute("kendaraans", kendaraanService.find());
        modelMap.addAttribute("lakalantas", lakalantas.cloneToSimpleLakalantas());
        return "lakalantas/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("jenisLakalantases", JenisLakalantas.values());
        modelMap.addAttribute("kondisiKendaraans", KondisiKendaraan.values());
        modelMap.addAttribute("penyidiks", penyidikService.find());
        modelMap.addAttribute("kendaraans", kendaraanService.find());
        modelMap.addAttribute("lakalantas", new SimpleLakalantas());
        return "lakalantas/input";
    }

    /***
     * Simpan hasil inputan
     * @param lakalantas
     * @param bindingResult
     * @param modelMap
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("lakalantas") SimpleLakalantas lakalantas, BindingResult bindingResult, ModelMap modelMap){

        if(lakalantas == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
           lakalantasService.save(lakalantas);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Lakalantas lakalantas) {
        try {
            lakalantasService.delete(lakalantas);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param lakalantas
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Lakalantas lakalantas) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("jenisLakalantases", JenisLakalantas.values());
        modelMap.addAttribute("kondisiKendaraans", KondisiKendaraan.values());
        modelMap.addAttribute("penyidiks", penyidikService.find());
        modelMap.addAttribute("kendaraans", kendaraanService.find());
        modelMap.addAttribute("lakalantas", lakalantas.cloneToSimpleLakalantas());
        return "lakalantas/input";
    }

    /**
     * Simpan hasil editan
     * @param lakalantas
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("lakalantas") SimpleLakalantas lakalantas, BindingResult bindingResult, ModelMap modelMap){

        if(lakalantas == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            lakalantasService.save(lakalantas);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Lakalantas.class, new LakalantasEditor(lakalantasService));
    }
}
