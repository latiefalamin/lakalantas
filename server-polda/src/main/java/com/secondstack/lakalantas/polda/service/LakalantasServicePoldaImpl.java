package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.Lakalantas;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.service.impl.LakalantasServiceImpl;
import com.secondstack.lakalantas.core.simple.SimpleLakalantas;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("lakalantasPoldaService")
public class LakalantasServicePoldaImpl extends LakalantasServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @Override
    public List<Lakalantas> find() {
        List<Polres> polreses = polresService.find();
        List<Lakalantas> lakalantass = new ArrayList<Lakalantas>();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "lakalantas").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<Lakalantas[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Lakalantas[].class);
            lakalantass.addAll(Arrays.asList(response.getBody()));
        }

        return lakalantass;
    }

    @Override
    public List<SimpleLakalantas> findSimple() {
        List<Polres> polreses = polresService.find();
        List<SimpleLakalantas> lakalantass = new ArrayList<SimpleLakalantas>();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "lakalantas?isSimple=true").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<SimpleLakalantas[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, SimpleLakalantas[].class);
            lakalantass.addAll(Arrays.asList(response.getBody()));
        }

        return lakalantass;
    }

    @Override
    public Lakalantas find(String id) {
        List<Polres> polreses = polresService.find();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "lakalantas?id=" + id).replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            try {
                ResponseEntity<Lakalantas> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Lakalantas.class);
                if(response.getBody() != null){
                    return response.getBody();
                }
            } catch (RestClientException e) {
                e.printStackTrace();
                continue;
            }
        }

        return null;
    }

    @Override
    public SimpleLakalantas findSimple(String id) {
        Lakalantas lakalantas = find(id);
        return  lakalantas == null ? null : lakalantas.cloneToSimpleLakalantas();
    }
}
