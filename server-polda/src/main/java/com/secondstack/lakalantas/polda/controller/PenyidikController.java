package com.secondstack.lakalantas.polda.controller;

import com.secondstack.lakalantas.core.domain.Penyidik;
import com.secondstack.lakalantas.core.editor.PenyidikEditor;
import com.secondstack.lakalantas.core.service.PenyidikService;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.simple.SimplePenyidik;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "penyidik")
public class PenyidikController {

    @Autowired
    @Qualifier("penyidikPoldaService")
    private PenyidikService penyidikService;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @RequestMapping(value = "navbar", method = RequestMethod.GET)
    public String navBar() {
        return "penyidik/navbar";
    }

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("penyidiks", penyidikService.findSimple());
        return "penyidik/list";
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("id") Penyidik penyidik) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("polreses", polresService.find());
        modelMap.addAttribute("penyidik", penyidik.cloneToSimplePenyidik());
        return "penyidik/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("polreses", polresService.find());
        modelMap.addAttribute("penyidik", new SimplePenyidik());
        return "penyidik/input";
    }

    /***
     * Simpan hasil inputan
     * @param penyidik
     * @param bindingResult
     * @param modelMap
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("penyidik") SimplePenyidik penyidik, BindingResult bindingResult, ModelMap modelMap){

        if(penyidik == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
           penyidikService.save(penyidik.cloneToPenyidik());
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Penyidik penyidik) {
        try {
            penyidikService.delete(penyidik);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param penyidik
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Penyidik penyidik) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("polreses", polresService.find());
        modelMap.addAttribute("penyidik", penyidik.cloneToSimplePenyidik());
        return "penyidik/input";
    }

    /**
     * Simpan hasil editan
     * @param penyidik
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("penyidik") SimplePenyidik penyidik, BindingResult bindingResult, ModelMap modelMap){

        if(penyidik == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            penyidikService.save(penyidik.cloneToPenyidik());
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Penyidik.class, new PenyidikEditor(penyidikService));
    }
}
