package com.secondstack.lakalantas.polda.controller;

import com.secondstack.lakalantas.core.domain.Asuransi;
import com.secondstack.lakalantas.core.editor.AsuransiEditor;
import com.secondstack.lakalantas.core.service.AsuransiService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "asuransi")
public class AsuransiController {

    @Autowired
    @Qualifier("asuransiPoldaService")
    private AsuransiService asuransiService;

    @RequestMapping(value = "navbar", method = RequestMethod.GET)
    public String navBar() {
        return "asuransi/navbar";
    }

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("asuransis", asuransiService.find());
        return "asuransi/list";
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("id") Asuransi asuransi) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("asuransi", asuransi);
        return "asuransi/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("asuransi", new Asuransi());
        return "asuransi/input";
    }

    /***
     * Simpan hasil inputan
     * @param asuransi
     * @param bindingResult
     * @param modelMap
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("asuransi") Asuransi asuransi, BindingResult bindingResult, ModelMap modelMap){

        if(asuransi == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
           asuransiService.save(asuransi);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Asuransi asuransi) {
        try {
            asuransiService.delete(asuransi);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param asuransi
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Asuransi asuransi) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("asuransi", asuransi);
        return "asuransi/input";
    }

    /**
     * Simpan hasil editan
     * @param asuransi
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("asuransi") Asuransi asuransi, BindingResult bindingResult, ModelMap modelMap){

        if(asuransi == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            asuransiService.save(asuransi);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Asuransi.class, new AsuransiEditor(asuransiService));
    }
}
