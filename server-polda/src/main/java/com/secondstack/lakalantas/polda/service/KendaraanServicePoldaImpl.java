package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.Kendaraan;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.service.impl.KendaraanServiceImpl;
import com.secondstack.lakalantas.core.simple.SimpleKendaraan;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("kendaraanPoldaService")
public class KendaraanServicePoldaImpl extends KendaraanServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @Override
    public List<Kendaraan> find() {
        List<Polres> polreses = polresService.find();
        List<Kendaraan> kendaraans = new ArrayList<Kendaraan>();

        for(Polres p:polreses){
            String url = (SystemUtils.HOST_WS_POLRES + "kendaraan").replace("host", p.getHost()).replace("port",p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<Kendaraan[]> response= restTemplate.exchange(url, HttpMethod.GET, httpEntity, Kendaraan[].class);
            kendaraans.addAll(Arrays.asList(response.getBody()));
        }

        return kendaraans;
    }

    @Override
    public List<SimpleKendaraan> findSimple() {
        List<Polres> polreses = polresService.find();
        List<SimpleKendaraan> kendaraans = new ArrayList<SimpleKendaraan>();

        for(Polres p:polreses){
            String url = (SystemUtils.HOST_WS_POLRES + "kendaraan?isSimple=true").replace("host", p.getHost()).replace("port",p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<SimpleKendaraan[]> response= restTemplate.exchange(url, HttpMethod.GET, httpEntity, SimpleKendaraan[].class);
            kendaraans.addAll(Arrays.asList(response.getBody()));
        }

        return kendaraans;
    }


    @Override
    public Kendaraan find(String id) {
        List<Polres> polreses = polresService.find();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "kendaraan?id=" + id).replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            try {
                ResponseEntity<Kendaraan> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Kendaraan.class);
                if(response.getBody() != null){
                    return response.getBody();
                }
            } catch (RestClientException e) {
                e.printStackTrace();
                continue;
            }
        }

        return null;
    }

    @Override
    public SimpleKendaraan findSimple(String id) {
        Kendaraan kendaraan = find(id);
        return  kendaraan == null ? null : kendaraan.cloneToSimpleKendaraan();
    }
}
