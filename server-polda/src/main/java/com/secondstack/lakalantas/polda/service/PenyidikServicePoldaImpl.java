package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.Penyidik;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.service.impl.PenyidikServiceImpl;
import com.secondstack.lakalantas.core.simple.SimplePenyidik;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("penyidikPoldaService")
public class PenyidikServicePoldaImpl extends PenyidikServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @Override
    public List<Penyidik> find() {
        List<Polres> polreses = polresService.find();
        List<Penyidik> penyidiks = new ArrayList<Penyidik>();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "penyidik").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<Penyidik[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Penyidik[].class);
            penyidiks.addAll(Arrays.asList(response.getBody()));
        }

        return penyidiks;
    }

    @Override
    public List<SimplePenyidik> findSimple() {
        List<Polres> polreses = polresService.find();
        List<SimplePenyidik> penyidiks = new ArrayList<SimplePenyidik>();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "penyidik?isSimple=true").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<SimplePenyidik[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, SimplePenyidik[].class);
            penyidiks.addAll(Arrays.asList(response.getBody()));
        }

        return penyidiks;
    }

    @Override
    public Penyidik find(String id) {
        List<Polres> polreses = polresService.find();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "penyidik?id=" + id).replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            try {
                ResponseEntity<Penyidik> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Penyidik.class);
                if(response.getBody() != null){
                    return response.getBody();
                }
            } catch (RestClientException e) {
                e.printStackTrace();
                continue;
            }
        }

        return null;
    }

    @Override
    public SimplePenyidik findSimple(String id) {
        Penyidik penyidik = find(id);
        return  penyidik == null ? null : penyidik.cloneToSimplePenyidik();
    }
}
