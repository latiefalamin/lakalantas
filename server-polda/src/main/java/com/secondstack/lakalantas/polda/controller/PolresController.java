package com.secondstack.lakalantas.polda.controller;

import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.editor.PolresEditor;
import com.secondstack.lakalantas.core.service.PolresService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "polres")
public class PolresController {

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @RequestMapping(value = "navbar", method = RequestMethod.GET)
    public String navBar() {
        return "polres/navbar";
    }

    /**
     * Ini select * all
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("polress", polresService.find());
        return "polres/list";
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("id") Polres polres) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("polres", polres);
        return "polres/input";
    }

    /**
     * kembalikan form inputan
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("polres", new Polres());
        return "polres/input";
    }

    /**
     * Simpan hasil inputan
     *
     * @param polres
     * @param bindingResult
     * @param modelMap
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@Valid @ModelAttribute("polres") Polres polres, BindingResult bindingResult, ModelMap modelMap) {

        if (polres == null) return "Gagal";
        if (bindingResult.hasErrors()) return "Gagal";

        try {
            polresService.save(polres);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Polres polres) {
        try {
            polresService.delete(polres);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     *
     * @param modelMap
     * @param polres
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id") Polres polres) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("polres", polres);
        return "polres/input";
    }

    /**
     * Simpan hasil editan
     *
     * @param polres
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@Valid @ModelAttribute("polres") Polres polres, BindingResult bindingResult, ModelMap modelMap) {

        if (polres == null) return "Gagal";
        if (bindingResult.hasErrors()) return "Gagal";

        try {
            polresService.update(polres);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Polres.class, new PolresEditor(polresService));
    }
}
