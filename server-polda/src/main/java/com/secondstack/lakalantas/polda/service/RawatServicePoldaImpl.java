package com.secondstack.lakalantas.polda.service;

import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.domain.Rawat;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.service.impl.RawatServiceImpl;
import com.secondstack.lakalantas.core.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("rawatPoldaService")
public class RawatServicePoldaImpl extends RawatServiceImpl {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("polresPoldaService")
    private PolresService polresService;

    @Override
    public List<Rawat> find() {
        List<Polres> polreses = polresService.find();
        List<Rawat> rawats = new ArrayList<Rawat>();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "rawat").replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            ResponseEntity<Rawat[]> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Rawat[].class);
            rawats.addAll(Arrays.asList(response.getBody()));
        }

        return rawats;
    }

    @Override
    public Rawat find(String id) {
        List<Polres> polreses = polresService.find();

        for (Polres p : polreses) {
            String url = (SystemUtils.HOST_WS_POLRES + "rawat?id=" + id).replace("host", p.getHost()).replace("port", p.getPort() + "");
            HttpEntity httpEntity = new HttpEntity(null);
            try {
                ResponseEntity<Rawat> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Rawat.class);
                if(response.getBody() != null){
                    return response.getBody();
                }
            } catch (RestClientException e) {
                e.printStackTrace();
                continue;
            }
        }

        return null;
    }

}
