<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/19/12
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:url value="/penyidik/edit" var="penyidikUrlEdit"/>
<c:url value="/penyidik" var="penyidikUrlDetail"/>
<c:url value="/penyidik" var="penyidikUrlDelete"/>

<table class="table" style="margin-top: -10px;">
    <thead>
    <tr>
        <th>NRP</th>
        <th>NAMA</th>
        <th>PANGKAT</th>
        <th>POLRES</th>
        <%--<th>ACTION</th>--%>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${penyidiks}" var="penyidik">

        <tr>
            <td><a href="#" onclick="detailClick('${penyidikUrlDetail}','${penyidik.nrp}')">${penyidik.nrp}</a></td>
            <td>${penyidik.nama}</td>
            <td>${penyidik.pangkat}</td>
            <td>${penyidik.polresNama}</td>
            <%--<td>--%>
                <%--<div>--%>
                    <%--<a class="btn minus" href="#"--%>
                       <%--onclick="buttonDeleteClick('${penyidikUrlDelete}','${penyidik.nrp}')"><i--%>
                            <%--class="icon-minus-sign" title="minus"></i></a>--%>
                    <%--<a class="btn edit" href="#" onclick="buttonEditClick('${penyidikUrlEdit}','${penyidik.nrp}')"><i--%>
                            <%--class="icon-edit" title="edit"></i></a>--%>
                <%--</div>--%>
            <%--</td>--%>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script type="text/javascript" src="<c:url value='/resources/js/edit.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/delete.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/detail.js'/>"></script>