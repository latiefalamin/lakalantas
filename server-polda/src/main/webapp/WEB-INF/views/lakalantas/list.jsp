<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/19/12
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:url value="/lakalantas/edit" var="lakalantasUrlEdit"/>
<c:url value="/lakalantas" var="lakalantasUrlDetail"/>
<c:url value="/lakalantas" var="lakalantasUrlDelete"/>

<table class="table" style="margin-top: -10px;">
    <thead>
    <tr>
        <th>ID</th>
        <th>JENIS</th>
        <th>KONDISI</th>
        <th>KENDARAAN</th>
        <th>PENYIDIK</th>
        <th>TEMPAT KEJADIAN</th>
        <th>WAKTU KEJADIAN</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${lakalantass}" var="lakalantas">

        <tr>
            <td><a href="#" onclick="detailClick('${lakalantasUrlDetail}','${lakalantas.id}')">${lakalantas.id}</a></td>
            <td>${lakalantas.jenisLakalantas}</td>
            <td>${lakalantas.kondisiKendaraan}</td>
            <td>${lakalantas.nopol}</td>
            <td>${lakalantas.penyidikNrp} ${lakalantas.penyidikNama}</td>
            <td>${lakalantas.tempatKejadian}</td>
            <td>${lakalantas.waktuKejadian}</td>

        </tr>
    </c:forEach>
    </tbody>
</table>

<script type="text/javascript" src="<c:url value='/resources/js/edit.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/delete.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/detail.js'/>"></script>