<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/20/12
  Time: 11:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="disabledInput" value="${event == 'DETAIL'}"/>
<c:url value="/lakalantas" var="lakalantas_url"/>


<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>

    <h3>${event} LAKALANTAS</h3>
</div>
<form:form id="formInputLakalantas" class="modal-form" action="${lakalantas_url}" method="${httpMethod}"
           modelAttribute="lakalantas">
    <div class="modal-body">

        <form:hidden path="id"/>
        <form:hidden path="polresKendaraanId"/>
        <form:hidden path="polresKendaraanNama"/>
        <form:hidden path="polresPenyidikId"/>
        <form:hidden path="polresPenyidikNama"/>

        <!--agar sejajar antara label dan inputan.-->
        <table>

                <%--Kendaraan--%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="selectKendaraan" class="input-xlarge">Kendaraan</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:select path="nopol" id="selectKendaraan" class="input-xlarge"
                                     disabled="${disabledInput}" >
                            <c:forEach var="kendaraan" items="${kendaraans}">
                                <form:option value="${kendaraan.nopol}" label="${kendaraan.nopol}"/>
                            </c:forEach>
                        </form:select>
                    </div>
                </td>
            </tr>

                <%--JENIS LAKALANTAS--%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="selectJenisLakalantas" class="input-xlarge">Jenis
                        Lakalantas</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:select path="jenisLakalantas" id="selectJenisLakalantas" class="input-xlarge"
                                     disabled="${disabledInput}">
                            <c:forEach var="jenisLakalantasItem" items="${jenisLakalantases}">
                                <form:option value="${jenisLakalantasItem}" label="${jenisLakalantasItem}"/>
                            </c:forEach>
                        </form:select>
                    </div>
                </td>
            </tr>

                <%--KONDISI KENDARAAN--%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="selectKondisiKendaraan" class="input-xlarge">Kondisi
                        Kendaraan</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:select path="kondisiKendaraan" id="selectKondisiKendaraan" class="input-xlarge"
                                     disabled="${disabledInput}">
                            <c:forEach var="kondisiKendaraanItem" items="${kondisiKendaraans}">
                                <form:option value="${kondisiKendaraanItem}" label="${kondisiKendaraanItem}"/>
                            </c:forEach>
                        </form:select>
                    </div>
                </td>
            </tr>

                <%--Penyidik--%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="selectPenyidikNrp" class="input-xlarge">Penyidik</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:select path="penyidikNrp" id="selectPenyidikNrp" class="input-xlarge"
                                     disabled="${disabledInput}">
                            <c:forEach var="penyidik" items="${penyidiks}">
                                <form:option value="${penyidik.nrp}" label="${penyidik.nrp} ${penyidik.nama}"/>
                            </c:forEach>
                        </form:select>
                    </div>
                </td>
            </tr>

                <%--waktuKejadian--%>
            <tr>
                <td>
                    <label class="control-label" for="textWaktuKejadian">Waktu Kejadian</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="waktuKejadian" id="textWaktuKejadian" class="input-xlarge" disabled="${disabledInput}"/>
                    </div>
                </td>
            </tr>

                <%--Tempat Kejadian--%>
            <tr>
                <td>
                    <label class="control-label" for="textTempatKejadian">Tempat Kejadian</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="tempatKejadian" id="textTempatKejadian" class="input-xlarge" disabled="${disabledInput}"/>
                    </div>
                </td>
            </tr>

        </table>

    </div>
    <div class="modal-footer">

            <%--tombol hanya muncul waktu input dan edit--%>
        <c:if test="${!disabledInput}">
            <%--<input type="submit" class="btn btn-primary" value="Simpan"/>--%>
            <input type="reset" class="btn" data-dismiss="modal" value="Batal">
        </c:if>

    </div>
</form:form>

<script type="text/javascript" src="<c:url value='/resources/js/form.js'/>"></script>