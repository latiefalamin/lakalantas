<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/19/12
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:url value="/korban/edit" var="korbanUrlEdit"/>
<c:url value="/korban" var="korbanUrlDetail"/>
<c:url value="/korban" var="korbanUrlDelete"/>

<table class="table" style="margin-top: -10px;">
    <thead>
    <tr>
        <th>ID</th>
        <th>LAKALANTAS</th>
        <th>NAMA</th>
        <th>TEMPAT KEJADIAN</th>
        <th>WAKTU KEJADIAN</th>
        <th>ALAMAT</th>
        <th>JENIS KELAMIN</th>
        <th>KONDISI</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${korbans}" var="korban">

        <tr>
            <td><a href="#" onclick="detailClick('${korbanUrlDetail}','${korban.id}')">${korban.id}</a></td>
            <td>${korban.lakalantas.id}</td>
            <td>${korban.nama}</td>
            <td>${korban.lakalantas.tempatKejadian}</td>
            <td>${korban.lakalantas.waktuKejadian}</td>
            <td>${korban.alamat}</td>
            <td>${korban.gender}</td>
            <td>${korban.kondisiKorban}</td>

        </tr>
    </c:forEach>
    </tbody>
</table>

<script type="text/javascript" src="<c:url value='/resources/js/edit.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/delete.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/detail.js'/>"></script>