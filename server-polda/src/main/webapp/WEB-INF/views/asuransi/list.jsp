<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/19/12
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:url value="/asuransi/edit" var="asuransiUrlEdit"/>
<c:url value="/asuransi" var="asuransiUrlDetail"/>
<c:url value="/asuransi" var="asuransiUrlDelete"/>

<table class="table" style="margin-top: -10px;">
    <thead>
    <tr>
        <th>ID</th>
        <th>NAMA</th>
        <th>ALAMAT</th>
        <th>ACTION</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${asuransis}" var="asuransi">

        <tr>
            <td><a href="#" onclick="detailClick('${asuransiUrlDetail}','${asuransi.id}')">${asuransi.id}</a></td>
            <td>${asuransi.nama}</td>
            <td>${asuransi.alamat}</td>
            <td>
                <div>
                    <a class="btn minus" href="#"
                       onclick="buttonDeleteClick('${asuransiUrlDelete}','${asuransi.id}')"><i
                            class="icon-minus-sign" title="minus"></i></a>
                    <a class="btn edit" href="#" onclick="buttonEditClick('${asuransiUrlEdit}','${asuransi.id}')"><i
                            class="icon-edit" title="edit"></i></a>
                </div>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script type="text/javascript" src="<c:url value='/resources/js/edit.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/delete.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/detail.js'/>"></script>