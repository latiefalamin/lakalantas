<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/20/12
  Time: 11:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="disabledInput" value="${event == 'DETAIL'}" />
<c:url value="/asuransi" var="asuransi_url"/>


<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>

    <h3>${event} ASURANSI</h3>
</div>
<form:form id="formInputAsuransi" class="modal-form" action="${asuransi_url}" method="${httpMethod}"
           modelAttribute="asuransi">
    <div class="modal-body">

        <!--agar sejajar antara label dan inputan.-->
        <table>

            <tr>
                <td>
                    <label class="control-label" for="textId">Id</label>
                </td>
                <td>
                    <div class="controls">
                        <%--&nbsp;&nbsp;--%>
                        <form:input path="id" id="textId" class="input-xlarge" disabled="${disabledInput}" readonly="true"/>
                        <%--&nbsp;--%>
                        <%--<span class="label label-info">*</span>--%>
                    </div>
                </td>
            </tr>

                <%--Nama--%>
            <tr>
                    <td>
                        <label class="control-label" for="textNama">Nama</label>
                    </td>
                    <td>
                        <div class="controls">
                            <%--&nbsp;&nbsp;--%>
                            <form:input path="nama" id="textNama" class="input-xlarge" disabled="${disabledInput}"/>
                            <%--&nbsp;--%>
                            <%--<span class="label label-info">*</span>--%>
                        </div>
                    </td>
            </tr>

                <%--Alamat--%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="textAlamat" class="input-xlarge">Alamat</label>
                </td>
                <td>
                    <div class="controls">
                        <%--&nbsp;&nbsp;--%>
                        <form:textarea path="alamat" id="textAlamat" rows="3" class="input-xlarge" disabled="${disabledInput}"/>
                        <%--&nbsp;--%>
                        <%--<span class="label label-info">*</span>--%>
                        <%--<form:errors delimiter="&lt;p/&gt;" class="badge badge-error" path="alamat"/>--%>
                    </div>
                </td>
            </tr>

        </table>

    </div>
    <div class="modal-footer">

        <%--tombol hanya muncul waktu input dan edit--%>
        <c:if test="${!disabledInput}" >
            <input type="submit" class="btn btn-primary" value="Simpan"/>
            <input type="reset" class="btn" data-dismiss="modal" value="Batal">
        </c:if>

    </div>
</form:form>

<script type="text/javascript" src="<c:url value='/resources/js/form.js'/>"></script>
