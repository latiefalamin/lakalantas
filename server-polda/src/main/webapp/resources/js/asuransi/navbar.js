/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var buttonAddAsuransi = $('#buttonAddAsuransi');
    var buttonRefreshAsuransi = $('#buttonRefreshAsuransi');
    var buttonSearchAsuransi = $('#buttonSearchAsuransi');

    buttonAddAsuransi.click(function(){
        $.get('/lakapolda/asuransi/new', function (data) {
            $('#myModalplus').html(data);
        });
        $('#myModalplus').modal('toggle');
    });

    //    defaultkan funsgsi reload ke reload asuransi
    reload = function(){
        $.ajax({
            url: '/lakapolda/asuransi',
            type: 'GET',
            success: function(result) {
                $('#bundleList').html(result);

            },
            error: function(xhr, status, error){
                alert(status);
            }
        });
    };

    buttonRefreshAsuransi.click(function () {
        reload();
    });
});