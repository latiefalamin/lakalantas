$(function(){
    var modalInput = 1;
    var test = $('.test');
    var minus = $('.minus');
    var plus = $('.plus');
    var edit = $('.edit');
    var form_block = $('#form_block');
    var form_hide = $('#form_hide');

    //menu
    var menuPolres = $('#accordianTogglePolres');
    var menuPenyidik = $('#accordianTogglePenyidik');
    var menuKendaraan = $('#accordianToggleKendaraan');
    var menuAsuransi = $('#accordianToggleAsuransi');
    var menuRumahSakit = $('#accordianToggleRumahSakit');
    var menuLakalantas = $('#accordianHeadingLakalantas');
    var menuKorban = $('#accordianToggleKorban');

    function fade(e){
        e.fadeIn();
    }

    test.click(function(){
        $('#myModaltest').modal('toggle');
        modalInput = 1;
        console.log(modalInput);

        $.get('/lakapolda/home/test', function(data){
            $('#idm').html(data)
        })
    });

    minus.click(function(){
        $('#myModalminus').modal('toggle');
        modalInput = 2;
        console.log(modalInput);
    });

    plus.click(function(){
        $('#myModalplus').modal('toggle');

        modalInput = 3;
        console.log(modalInput);
    });

    edit.click(function(){
        $('#myModaledit').modal('toggle');

        modalInput = 4;
        console.log(modalInput);
    });

    form_block.click(function(){
//        form_hide.toggle(fade(form_hide));
        form_hide.toggle();
    });


//    action menu

    menuPolres.click(function () {
        $.get('/lakapolda/polres/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolda/polres', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuPenyidik.click(function () {
        $.get('/lakapolda/penyidik/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolda/penyidik', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuKendaraan.click(function () {
        $.get('/lakapolda/kendaraan/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolda/kendaraan', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuAsuransi.click(function () {
        $.get('/lakapolda/asuransi/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolda/asuransi', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuRumahSakit.click(function () {
        $.get('/lakapolda/rumahSakit/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolda/rumahSakit', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuLakalantas.click(function () {
        $.get('/lakapolda/lakalantas/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolda/lakalantas', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuKorban.click(function () {
        $.get('/lakapolda/korban/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolda/korban', function (data) {
            $('#bundleList').html(data);
        });
    });
});

var reload = function(){};