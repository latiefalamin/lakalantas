/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var buttonAddKorban = $('#buttonAddKorban');
    var buttonRefreshKorban = $('#buttonRefreshKorban');


    buttonAddKorban.click(function(){
        console.log('add korban');
        $.get('/lakapolda/korban/new', function (data) {
            $('#myModalplus').html(data);
        });
        $('#myModalplus').modal('toggle');
    });

    //    defaultkan funsgsi reload ke reload korban
    reload = function(){
        $.ajax({
            url: '/lakapolda/korban',
            type: 'GET',
            success: function(result) {
                $('#bundleList').html(result);

            },
            error: function(xhr, status, error){
                alert(status);
            }
        });
    };

    buttonRefreshKorban.click(function () {
        reload();
    });
});