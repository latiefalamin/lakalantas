/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var buttonAddLakalantas = $('#buttonAddLakalantas');
    var buttonRefreshLakalantas = $('#buttonRefreshLakalantas');
    var buttonSearchLakalantas = $('#buttonSearchLakalantas');

    buttonAddLakalantas.click(function(){
        $.get('/lakapolda/lakalantas/new', function (data) {
            $('#myModalplus').html(data);
        });
        $('#myModalplus').modal('toggle');
    });

    //    defaultkan funsgsi reload ke reload lakalantas
    reload = function(){
        $.ajax({
            url: '/lakapolda/lakalantas',
            type: 'GET',
            success: function(result) {
                $('#bundleList').html(result);

            },
            error: function(xhr, status, error){
                alert(status);
            }
        });
    };

    buttonRefreshLakalantas.click(function () {
        reload();
    });
});