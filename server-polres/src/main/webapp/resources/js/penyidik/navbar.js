/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var buttonAddPenyidik = $('#buttonAddPenyidik');
    var buttonRefreshPenyidik = $('#buttonRefreshPenyidik');
    var buttonSearchPenyidik = $('#buttonSearchPenyidik');

    buttonAddPenyidik.click(function(){
        $.get('/lakapolres/penyidik/new', function (data) {
            $('#myModalplus').html(data);
        });
        $('#myModalplus').modal('toggle');
    });

    //    defaultkan funsgsi reload ke reload penyidik
    reload = function(){
        $.ajax({
            url: '/lakapolres/penyidik',
            type: 'GET',
            success: function(result) {
                $('#bundleList').html(result);

            },
            error: function(xhr, status, error){
                alert(status);
            }
        });
    };

    buttonRefreshPenyidik.click(function () {
        reload();
    });
});