/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/20/12
 * Time: 9:36 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * data yang akan diolah
 */
var korban;

var incrementIdTagRawat = 0;
var idTagRawats = new Array();

var incrementIdTagSantunan = 0;
var idTagSantunans = new Array();

var tagRowRawat =
    "<tr id='rowRawat' >" +
        "<td><input type='text' id='selectRumahSakitIdTag' onchange='changeValueRawat(idTag)' style='width: 65px' value='valueRumahSakit'/></td>" +
        "<td><input type='text' id='textBiayaIdTag' onchange='changeValueRawat(idTag)' value='valueBiaya' style='width: 70px'/></td>" +
        "<td><input type='text' id='textWaktuMasukIdTag' onchange='changeValueRawat(idTag)' value='valueWaktuMasuk' style='width: 130px'/></td>" +
        "<td><input type='text' id='textWaktuKeluarIdTag' onchange='changeValueRawat(idTag)' value='valueWaktuKeluar' style='width: 130px'/></td>" +
        "<td><button type='button' class='btn button-minus' style='margin-top: -9px' onclick='deleteRawat(idTag)'><i class='icon-minus-sign' title='minus'></i></button></td>" +
        "</tr>";

var tagRowSantunan =
    "<tr id='rowSantunan'>" +
        "<td><input type='text' id='selectAsuransiIdTag' onchange='changeValueSantunan(idTag)' style='width: 65px' value='valueAsuransi'/></td>" +
        "<td><input type='text' id='textBiayaIdTag' onchange='changeValueSantunan(idTag)' value='valueBiaya' style='width: 70px'/></td>" +
        "<td><button type='button' class='btn button-minus' style='margin-top: -9px' onclick='deleteSantunan(idTag)'><i class='icon-minus-sign' title='minus'></i></button></td>" +
        "</tr>";

/**
 * kejadian ketika submit korban
 */
$('form').submit(function (event) {
    event.preventDefault();

    var data = $(this).serializeObject();
    var action = $(this).attr("action");
    var method = $(this).attr("method");

//        console.log('data = ' + data);
//        console.log('action = ' + action);
//        console.log('method = ' + method);
//    console.info(data);

    korban.nama = data.nama;
    korban.alamat = data.alamat;
    korban.gender = data.gender;
    korban.kondisiKorban = data.kondisiKorban;
    korban.nik = data.nik;
    korban.umur = data.umur;
    korban.lakalantas.id = $("#selectLakalantas").val();

    console.info(korban);

    var success = false;

    $.ajax({
        url:action,
        type:method,
        data:JSON.stringify(korban),
        contentType:"application/json; charset=utf-8",
        success:function (result) {
            success = (result + "").toLowerCase() == "sukses" ? true : false;
            alert(result);

            //jika sukses matikan dialog
            if (success) {
                $('#myModalplus').modal('hide');
                reload();
            }

        },
        error:function (xhr, status, error) {
            alert(status);
        }
    });

    return success;
});

/**
 * kejadian ketika tombol + santunan ditekan
 */
$('#buttonAddRawat').click(function () {
    addRawat(null);
    return false;
});

/**
 * kejadian ketika tombol + santunan ditekan
 */
$('#buttonAddSantunan').click(function () {
    addSantunan(null);
    return false;
});

/**
 * inisialisasi awal korban
 * @param korban
 */
function init(t) {

    console.log('korbanInit');
    console.info(t);

    //inisialisasi awal korban
    korban = t;
    if (korban == null) {
        korban = {
            id:null,
            alamat:null,
            gender:null,
            kondisiKorban:null,
            nama:null,
            nik:null,
            umur:null,
            lakalantas: {
                id : null
            },
            rawats : new Array(),
            santunans : new Array()
        }
    }
    if(korban.lakalantas == null){
        korban.lakalantas = {
            id : null
        }
    }
    if (korban.rawats == null) {
        korban.rawats = new Array();
    }
    if (korban.santunans == null) {
        korban.santunans = new Array();
    }

    //tambahkan rawats dan santunans
    for (var i = 0; i < korban.rawats.length; i++) {
        addRawat(korban.rawats[i]);
    }
    for (var i = 0; i < korban.santunans.length; i++) {
        addSantunan(korban.santunans[i]);
    }

    console.info(korban);
}

/**
 * tambah rawat
 * @param rawat
 */
function addRawat(rawat) {
    console.log("addRawat");

    //id tag rawat baru tambahkan ke daftar index id tag rawat
    var newIdTagRawat = incrementIdTagRawat++;
    idTagRawats.push(newIdTagRawat);

    //beri nilai default untuk rawat baru
    if (korban.rawats == null)
        korban.rawats = new Array();
    var newRawat = (rawat == null ?
    {
        id:null,
        biaya:null,
        waktuKeluar:null,
        waktuMasuk:null,
        rumahSakit:null
    } : rawat);
    if(newRawat.rumahSakit == null){
        newRawat.rumahSakit = {id : null};
    }
    if(rawat == null){
        korban.rawats.push(newRawat);
    }

    //buat tag baru
    var nowTagRowRawat = tagRowRawat
        .replace("valueRumahSakit", newRawat.rumahSakit == null ? "" : newRawat.rumahSakit.id)
        .replace("valueBiaya", newRawat.biaya == null ? "0" : newRawat.biaya + "")
        .replace("valueWaktuMasuk", newRawat.waktuMasuk == null ? "" : newRawat.waktuMasuk + "")
        .replace("valueWaktuKeluar", newRawat.waktuKeluar == null ? "" : newRawat.waktuKeluar + "")
        .replace(/idtag/gi, newIdTagRawat + "");

    //tambah tag baru dibawah
    $('#listRawat').append(nowTagRowRawat);

    console.info(korban);
    console.info(idTagRawats);
}

/**
 * tambah santunan
 * @param santunannan
 */
function addSantunan(santunan) {
    console.log("addSantunan ");

    //id tag santunan baru tambahkan ke daftar index id tag santunan
    var newIdTagSantunan = incrementIdTagSantunan++;
    idTagSantunans.push(newIdTagSantunan);

    //beri nilai default untuk santunan baru
    if (korban.santunans == null)
        korban.santunans = new Array();
    var newSantunan = santunan == null ?
    {
        id:null,
        biayaSantunan:null,
        asuransi : null
    } : santunan;
    if(newSantunan.asuransi == null){
        newSantunan.asuransi = {id : null};
    }
    if(santunan == null){
        korban.santunans.push(newSantunan);
    }

    //buat tag baru
    var nowTagRowSantunan = tagRowSantunan
        .replace("valueAsuransi", newSantunan.asuransi == null ? "" : newSantunan.asuransi.id)
        .replace("valueBiaya", newSantunan.biaya == null ? "0" : newSantunan.biaya + "")
        .replace(/idtag/gi, newIdTagSantunan + "");

    //tambah tag baru dibawah
    $('#listSantunan').append(nowTagRowSantunan);

    console.info(korban);
    console.info(idTagSantunans);
}

/**
 * hapus rawat dari modal
 * @param idTagRawat
 */
function deleteRawat(idTagRawat) {
    var i = idTagRawats.indexOf(idTagRawat);
    console.log("deleteRawat " + idTagRawat + " = " + i);

    if (i >= 0) {
        korban.rawats.splice(i, 1);
        idTagRawats.splice(i, 1);
        $("#rowRawat" + idTagRawat).remove();
    }
    console.info(korban);
    console.info(idTagRawats);
}
/**
 * hapus santunan dari modal
 * @param idTagRawat
 */
function deleteSantunan(idTagSantunan) {
    var i = idTagRawats.indexOf(idTagSantunan);
    console.log("deleteSantunan " + idTagSantunan + " = " + i);

    if (i >= 0) {
        korban.santunans.splice(i, 1);
        idTagSantunans.splice(i, 1);
        $("#rowSantunan" + idTagSantunan).remove();
    }
    console.info(korban);
    console.info(idTagSantunans);
}

/**
 * rubah value rawat
 * @param idTagRawat
 */
function changeValueRawat(idTagRawat) {
    var i = idTagRawats.indexOf(idTagRawat);
    console.log("changeValueRawats " + idTagRawat + " = " + i);

    if (korban != null && korban.rawats != null && i >= 0) {
        korban.rawats[i].biaya = $("#textBiaya" + idTagRawat).val();
        korban.rawats[i].waktuKeluar = $("#textWaktuKeluar" + idTagRawat).val();
        korban.rawats[i].waktuMasuk =  $("#textWaktuMasuk" + idTagRawat).val();
        korban.rawats[i].rumahSakit.id = $("#selectRumahSakit" + idTagRawat).val();
    }

    console.info(korban);
}

/**
 * rubah value santunan
 * @param idTagSantunan
 */
function changeValueSantunan(idTagSantunan) {
    var i = idTagSantunans.indexOf(idTagSantunan);
    console.log("changeValueSantunans " + idTagSantunan + " = " + i);

    if (korban != null && korban.santunans != null && i >= 0) {
        korban.santunans[i].biayaSantunan = $("#textBiaya" + idTagSantunan).val();
        korban.santunans[i].asuransi.id = $("#selectAsuransi" + idTagSantunan).val();
    }

    console.info(korban);
    console.info(idTagSantunans);
}

/**
 * konversi format string dd/MM/YYYY hh:mm:ss ke time
 * @param s
 */
function stringToDate(s){
    console.log('stringToDate ' + s)
    if(s == null) return null;

    var patternTime= /\d{1,2}[/]\d{1,2}[/]\d{4,4}\s+\d{1,2}[:]\d{1,2}[:]\d{1,2}/g;
    var timeMatch= s.match(patternTime);
    console.info(timeMatch);
    if(timeMatch == null) return null;

    var ymd = timeMatch[0].split(" ")[0].split("/");
    var hms = timeMatch[0].split(" ")[1].split(":");
    var year = ymd[2];
    var month= ymd[1];
    var day = ymd[0];
    var hour = hms[0];
    var minute = hms[1];
    var second = hms[2];

    console.log(year);
    console.log(month);
    console.log(day);
    console.log(hour);
    console.log(minute);
    console.log(second);

    return new Date(year, month, day, hour, minute, second);
}