/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var buttonAddPolres = $('#buttonAddPolres');
    var buttonRefreshPolres = $('#buttonRefreshPolres');
    var buttonSearchPolres = $('#buttonSearchPolres');

    buttonAddPolres.click(function(){
        $.get('/lakapolres/polres/new', function (data) {
            $('#myModalplus').html(data);
        });
        $('#myModalplus').modal('toggle');
    });

    //    defaultkan funsgsi reload ke reload polres
    reload = function(){
        $.ajax({
            url: '/lakapolres/polres',
            type: 'GET',
            success: function(result) {
                $('#bundleList').html(result);

            },
            error: function(xhr, status, error){
                alert(status);
            }
        });
    };

    buttonRefreshPolres.click(function () {
        reload();
    });
});