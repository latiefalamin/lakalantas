$(function(){
    var modalInput = 1;
    var test = $('.test');
    var minus = $('.minus');
    var plus = $('.plus');
    var edit = $('.edit');
    var form_block = $('#form_block');
    var form_hide = $('#form_hide');

    //menu
    var menuPolres = $('#accordianTogglePolres');
    var menuPenyidik = $('#accordianTogglePenyidik');
    var menuKendaraan = $('#accordianToggleKendaraan');
    var menuAsuransi = $('#accordianToggleAsuransi');
    var menuRumahSakit = $('#accordianToggleRumahSakit');
    var menuLakalantas = $('#accordianHeadingLakalantas');
    var menuKorban = $('#accordianToggleKorban');

    function fade(e){
        e.fadeIn();
    }

    test.click(function(){
        $('#myModaltest').modal('toggle');
        modalInput = 1;
        console.log(modalInput);

        $.get('/lakapolres/home/test', function(data){
            $('#idm').html(data)
        })
    });

    minus.click(function(){
        $('#myModalminus').modal('toggle');
        modalInput = 2;
        console.log(modalInput);
    });

    plus.click(function(){
        $('#myModalplus').modal('toggle');

        modalInput = 3;
        console.log(modalInput);
    });

    edit.click(function(){
        $('#myModaledit').modal('toggle');

        modalInput = 4;
        console.log(modalInput);
    });

    form_block.click(function(){
//        form_hide.toggle(fade(form_hide));
        form_hide.toggle();
    });


//    action menu

    menuPolres.click(function () {
        $.get('/lakapolres/polres/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolres/polres', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuPenyidik.click(function () {
        $.get('/lakapolres/penyidik/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolres/penyidik', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuKendaraan.click(function () {
        $.get('/lakapolres/kendaraan/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolres/kendaraan', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuAsuransi.click(function () {
        $.get('/lakapolres/asuransi/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolres/asuransi', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuRumahSakit.click(function () {
        $.get('/lakapolres/rumahSakit/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolres/rumahSakit', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuLakalantas.click(function () {
        $.get('/lakapolres/lakalantas/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolres/lakalantas', function (data) {
            $('#bundleList').html(data);
        });
    });

    menuKorban.click(function () {
        $.get('/lakapolres/korban/navbar', function (dataNavBar) {
            $('#bundleNavbar').html(dataNavBar);
        });
        $.get('/lakapolres/korban', function (data) {
            $('#bundleList').html(data);
        });
    });
});

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var reload = function(){};