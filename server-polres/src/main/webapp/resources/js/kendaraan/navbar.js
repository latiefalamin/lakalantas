/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var buttonAddKendaraan = $('#buttonAddKendaraan');
    var buttonRefreshKendaraan = $('#buttonRefreshKendaraan');
    var buttonSearchKendaraan = $('#buttonSearchKendaraan');

    buttonAddKendaraan.click(function(){
        $.get('/lakapolres/kendaraan/new', function (data) {
            $('#myModalplus').html(data);
        });
        $('#myModalplus').modal('toggle');
    });

    //    defaultkan funsgsi reload ke reload kendaraan
    reload = function(){
        $.ajax({
            url: '/lakapolres/kendaraan',
            type: 'GET',
            success: function(result) {
                $('#bundleList').html(result);

            },
            error: function(xhr, status, error){
                alert(status);
            }
        });
    };

    buttonRefreshKendaraan.click(function () {
        reload();
    });
});