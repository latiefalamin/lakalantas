/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 12:23 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {

    var buttonAddRumahSakit = $('#buttonAddRumahSakit');
    var buttonRefreshRumahSakit = $('#buttonRefreshRumahSakit');
    var buttonSearchRumahSakit = $('#buttonSearchRumahSakit');

    buttonAddRumahSakit.click(function(){
        $.get('/lakapolres/rumahSakit/new', function (data) {
            $('#myModalplus').html(data);
        });
        $('#myModalplus').modal('toggle');
    });

//    defaultkan funsgsi reload ke reload rumah sakit
    reload = function(){
        $.ajax({
            url: '/lakapolres/rumahSakit',
            type: 'GET',
            success: function(result) {
                $('#bundleList').html(result);

            },
            error: function(xhr, status, error){
                alert(status);
            }
        });
    };

    buttonRefreshRumahSakit.click(function () {
        reload();
    });
});
