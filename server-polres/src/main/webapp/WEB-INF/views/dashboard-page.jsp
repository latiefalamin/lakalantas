<%--
  Created by IntelliJ IDEA.
  User: bahrie
  Date: 3/15/12
  Time: 2:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Kecelakaan Lalu Lintas Polres</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>

    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }

        .sidebar-nav {
            padding: 9px 0;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap-responsive.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap-stack.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/jquery.ui.all.css'/>"/>
    <script src="<c:url value='/resources/js/jquery.js'/>"></script>

</head>
<%--ondoel--%>
<body>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="i-bar"></span>
                <span class="i-bar"></span>
                <span class="i-bar"></span>
            </a>
            <a class="brand" href="#">Kecelakaan Lalu Lintas Polres</a>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <div class="well sidebar-nav sideContent">
                <ul class="nav nav-list">
                    <!--<li class="nav-header">Menu</li>-->
                    <li class="active"><a href="#" style="text-align: center">Menu</a></li>
                    <li>

                        <div id="accordion2" class="accordion">
                            <div class="accordion-group batasAtas">

                                <%--menu kepolisian--%>
                                <div class="accordion-heading" id="accordianHeadingKepolisian">
                                    <a class="accordion-toggle warnaFont" href="#" data-parent="#accordion2"
                                       data-toggle="collapse"> Kepolisian </a>
                                </div>
                                <div id="collapseKepolisian" class="accordion-body in collapse" style="height: auto;">
                                    <div class="accordion-inner bgSubmenu">
                                        <ul class="nav nav-tabs nav-stacked">
                                            <li><a id="accordianTogglePolres" class="accordion-toggle warnaFontSubmenu"
                                                   href="#collapsePolres"
                                                   data-parent="#accordianHeadingKepolisian"
                                                   data-toggle="collapse">Polres</a></li>
                                            <li><a id="accordianTogglePenyidik"
                                                   class="accordion-toggle warnaFontSubmenu"
                                                   href="#collapsePenyidik"
                                                   data-parent="#accordianHeadingKepolisian"
                                                   data-toggle="collapse">Penyidik</a></li>
                                            <li><a id="accordianToggleKendaraan"
                                                   class="accordion-toggle warnaFontSubmenu"
                                                   href="#collapseKendaraan"
                                                   data-parent="#accordianHeadingKepolisian"
                                                   data-toggle="collapse">Kendaraan</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <%--menu instansi--%>
                                <div class="accordion-heading" id="accordianHeadingInstansi">
                                    <a class="accordion-toggle warnaFont" href="#" data-parent="#accordion2"
                                       data-toggle="collapse"> Instansi </a>
                                </div>
                                <div id="collapseInstansi" class="accordion-body in collapse" style="height: auto;">
                                    <div class="accordion-inner bgSubmenu">
                                        <ul class="nav nav-tabs nav-stacked">
                                            <li><a id="accordianToggleAsuransi"
                                                   class="accordion-toggle warnaFontSubmenu"
                                                   href="#collapseAsuransi"
                                                   data-parent="#accordianHeadingInstansi"
                                                   data-toggle="collapse">Asuransi</a></li>
                                            <li><a id="accordianToggleRumahSakit"
                                                   class="accordion-toggle warnaFontSubmenu"
                                                   href="#collapseRumahSakit"
                                                   data-parent="#accordianHeadingInstansi"
                                                   data-toggle="collapse">Rumah Sakit</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <%--menu lakalantas--%>
                                <div class="accordion-heading" id="accordianHeadingLakalantas">
                                    <a class="accordion-toggle warnaFont" href="#" data-parent="#accordion2"
                                       data-toggle="collapse"> Lakalantas </a>
                                </div>
                                <div id="collapseLakalantas" class="accordion-body in collapse" style="height: auto;">
                                    <div class="accordion-inner bgSubmenu">
                                        <ul class="nav nav-tabs nav-stacked">
                                            <li><a id="accordianToggleKorban"
                                                   class="accordion-toggle warnaFontSubmenu"
                                                   href="#collapseKorban"
                                                   data-parent="#accordianHeadingLakalantas"
                                                   data-toggle="collapse">Korban</a></li>
                                        </ul>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!--/.well -->
        </div>
        <!--/span-->
        <div class="span10">
            <div id="heroUnit" class="hero-unit" style="height: auto; padding: 30px">

                <div id="bundleNavbar"> <!--bundle nav-->

                </div>


                <!--list-list-->
                <div id="bundleList"> <!--bundle table-->

                </div>

            </div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->

    <div id="myModalminus" class="modal hide fade in">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>

            <h6>Konfirmasi</h6>
        </div>
        <div id="modalMinusBody" class="modal-body">

        </div>
        <%--<div class="form-actions">--%>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Ok</button>
        </div>
        <%--</div>--%>
        <%--</fieldset>--%>
        </form>
    </div>

    <div id="myModalplus" class="modal hide fade in" style="width: auto;">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>
            <h6>INPUT EXAMPLE</h6>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
            <a class="btn btn-primary" href="#">Save changes</a>
            <a class="btn" data-dismiss="modal" href="#">Close</a>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; 2ndStack 2012</p>
    </footer>

</div>

<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap-stack.js'/>"></script>
</body>
</html>