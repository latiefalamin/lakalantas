<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/20/12
  Time: 11:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="disabledInput" value="${event == 'DETAIL'}"/>
<c:url value="/korban" var="korban_url"/>


<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>

    <h3>${event} KORBAN</h3>
</div>
<form:form id="formInputKorban" class="modal-form" action="${korban_url}" method="${httpMethod}"
           modelAttribute="korban">
    <div class="modal-body">

        <!--agar sejajar antara label dan inputan.-->
        <table>

                <%--lakalantas--%>
            <tr>
                <td>
                    <label class="control-label" for="selectLakalantas">Lakalantas</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <select id="selectLakalantas" class="input-xlarge"
                            <%--disabled="${disabledInput}"--%>>
                            <c:forEach var="lakalantasItem" items="${lakalantases}">
                                <option value="${lakalantasItem.id}" label="${lakalantasItem.id}"/>
                            </c:forEach>
                        </select>
                    </div>
                </td>
            </tr>

                <%--nama--%>
            <tr>
                <td>
                    <label class="control-label" for="textNama">Nama</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="nama" id="textNama" class="input-xlarge" disabled="${disabledInput}"/>
                    </div>
                </td>
            </tr>

                <%--NIK--%>
            <tr>
                <td>
                    <label class="control-label" for="textNik">NIK</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="nik" id="textNik" class="input-xlarge" disabled="${disabledInput}"/>
                    </div>
                </td>
            </tr>

                <%--GENDER--%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="selectGender" class="input-xlarge">Gender</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:select path="gender" id="selectGender" class="input-xlarge"
                                     disabled="${disabledInput}">
                            <c:forEach var="genderItem" items="${genders}">
                                <form:option value="${genderItem}" label="${genderItem}"/>
                            </c:forEach>
                        </form:select>
                    </div>
                </td>
            </tr>

                <%--UMUR--%>
            <tr>
                <td>
                    <label class="control-label" for="textUmur">Umur</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="umur" id="textUmur" class="input-xlarge" disabled="${disabledInput}"/>
                    </div>
                </td>
            </tr>

                <%-- KONDISI KORBAN --%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="selectKondisiKorban" class="input-xlarge">Kondisi Korban</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:select path="kondisiKorban" id="selectKondisiKorban" class="input-xlarge"
                                     disabled="${disabledInput}">
                            <c:forEach var="kondisiKorbanItem" items="${kondisiKorbans}">
                                <form:option value="${kondisiKorbanItem}" label="${kondisiKorbanItem}"/>
                            </c:forEach>
                        </form:select>
                    </div>
                </td>
            </tr>

                <%--Alamat--%>
            <tr>
                <td>
                    <label class="control-label" for="textNik">Alamat</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="alamat" id="textAlamat" class="input-xlarge" disabled="${disabledInput}"/>
                    </div>
                </td>
            </tr>

                <%--rawats--%>
            <tr>
                <td>
                    <label class="control-label">Rawat</label>
                </td>
                <td>
                    &nbsp;&nbsp;
                    <button type="button" class="btn" id="buttonAddRawat"
                            style="margin-top:-5px;display: none;margin-right: 290px;display: inline-block;margin-right: 2px">
                        <i class="icon-plus"></i></button>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <thead>
                        <tr>
                            <td>Rumah Sakit</td>
                            <td>Biaya</td>
                            <td>Waktu Masuk</td>
                            <td>Waktu Keluar</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody id="listRawat">

                        </tbody>
                    </table>
                </td>
            </tr>

                <%--santunans--%>
            <tr>
                <td>
                    <label class="control-label">Santunan</label>
                </td>
                <td>
                    &nbsp;&nbsp;
                    <button type="button" class="btn" id="buttonAddSantunan"
                            style="margin-top:-5px;display: none;margin-right: 290px;display: inline-block;margin-right: 2px">
                        <i class="icon-plus"></i></button>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <thead>
                        <tr>
                            <td>Asuransi</td>
                            <td>Biaya</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody id="listSantunan">

                        </tbody>
                    </table>
                </td>
            </tr>

        </table>

    </div>
    <div class="modal-footer">

            <%--tombol hanya muncul waktu input dan edit--%>
        <c:if test="${!disabledInput}">
            <input type="submit" class="btn btn-primary" value="Simpan"/>
            <input type="reset" class="btn" data-dismiss="modal" value="Batal">
        </c:if>

    </div>
</form:form>

<script type="text/javascript" src="<c:url value='/resources/js/korban/form.js'/>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        <%--init(${korbanJson});--%>
        korban = ${korbanJson};
        if (korban == null) {
            korban = {
                id:null,
                alamat:null,
                gender:null,
                kondisiKorban:null,
                nama:null,
                nik:null,
                umur:null,
                lakalantas:{
                    id:null
                },
                rawats:new Array(),
                santunans:new Array()
            }
        }
        if (korban.lakalantas == null) {
            korban.lakalantas = {
                id:null
            }
        }
        if (korban.rawats == null) {
            korban.rawats = new Array();
        }
        if (korban.santunans == null) {
            korban.santunans = new Array();
        }

        //tambahkan rawats dan santunans
        var rawatsLength = korban.rawats.length;
        for (var i = 0; i < rawatsLength; i++) {
            addRawat(korban.rawats[i]);
        }
        var santunasLength =  korban.santunans.length;
        for (var i = 0; i < santunasLength; i++) {
            addSantunan(korban.santunans[i]);
        }

        console.log(korban);
    });
</script>