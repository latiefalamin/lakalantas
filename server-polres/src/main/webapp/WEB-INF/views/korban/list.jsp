<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/19/12
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:url value="/korban/edit" var="korbanUrlEdit"/>
<c:url value="/korban" var="korbanUrlDetail"/>
<c:url value="/korban" var="korbanUrlDelete"/>

<table class="table" style="margin-top: -10px;">
    <thead>
    <tr>
        <th>ID</th>
        <th>LAKALANTAS</th>
        <th>NAMA</th>
        <th>TEMPAT KEJADIAN</th>
        <th>WAKTU KEJADIAN</th>
        <th>ALAMAT</th>
        <th>JENIS KELAMIN</th>
        <th>KONDISI</th>
        <th>ACTION</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${korbans}" var="korban">

        <tr>
            <td><a href="#" onclick="detailClick('${korbanUrlDetail}','${korban.id}')">${korban.id}</a></td>
            <td>${korban.lakalantas.id}</td>
            <td>${korban.nama}</td>
            <td>${korban.lakalantas.tempatKejadian}</td>
            <td>${korban.lakalantas.waktuKejadian}</td>
            <td>${korban.alamat}</td>
            <td>${korban.gender}</td>
            <td>${korban.kondisiKorban}</td>
            <td>
                <div>
                    <a class="btn minus" href="#"
                       onclick="buttonDeleteClick('${korbanUrlDelete}','${korban.id}')"><i
                            class="icon-minus-sign" title="minus"></i></a>
                    <a class="btn edit" href="#" onclick="buttonEditClick('${korbanUrlEdit}','${korban.id}')"><i
                            class="icon-edit" title="edit"></i></a>
                </div>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script type="text/javascript" src="<c:url value='/resources/js/edit.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/delete.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/detail.js'/>"></script>