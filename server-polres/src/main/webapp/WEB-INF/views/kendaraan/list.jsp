<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/19/12
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:url value="/kendaraan/edit" var="kendaraanUrlEdit"/>
<c:url value="/kendaraan" var="kendaraanUrlDetail"/>
<c:url value="/kendaraan" var="kendaraanUrlDelete"/>

<table class="table" style="margin-top: -10px;">
    <thead>
    <tr>
        <th>NOPOL</th>
        <th>JENIS KENDARAAN</th>
        <th>MERK</th>
        <th>POLRES</th>
        <th>ACTION</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${kendaraans}" var="kendaraan">

        <tr>
            <td><a href="#" onclick="detailClick('${kendaraanUrlDetail}','${kendaraan.nopol}')">${kendaraan.nopol}</a></td>
            <td>${kendaraan.jenisKendaraan}</td>
            <td>${kendaraan.merk}</td>
            <td>${kendaraan.polres.nama}</td>
            <td>
                <div>
                    <a class="btn minus" href="#"
                       onclick="buttonDeleteClick('${kendaraanUrlDelete}','${kendaraan.nopol}')"><i
                            class="icon-minus-sign" title="minus"></i></a>
                    <a class="btn edit" href="#" onclick="buttonEditClick('${kendaraanUrlEdit}','${kendaraan.nopol}')"><i
                            class="icon-edit" title="edit"></i></a>
                </div>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script type="text/javascript" src="<c:url value='/resources/js/edit.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/delete.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/detail.js'/>"></script>