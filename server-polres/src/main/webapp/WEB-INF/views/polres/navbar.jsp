<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/19/12
  Time: 12:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="navbar">
    <div class="navbar-inner">
        <div class="container">

            <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <!-- Be sure to leave the brand out there if you want it shown -->
            <a class="brand" href="#">POLRES</a>

            <!-- Everything you want hidden at 940px or less, place within here -->
            <div class="nav-collapse">

                <ul class="nav pull-right">
                    <%--<li class="">--%>
                        <%--<a id="buttonAddPolres" class="plus" href="#"><i class="icon-plus-sign icon-white" title="plus"></i></a>--%>
                    <%--</li>--%>
                    <li>
                        <a id="buttonRefreshPolres" href="#"><i class="icon-refresh icon-white" title="refresh"></i></a>
                    </li>
                    <%--<li>--%>
                        <%--<a id="buttonSearchPolres" href="#" class="plus"><i class="icon-search icon-white" title="search"></i></a>--%>
                    <%--</li>--%>

                </ul>

                <form class="navbar-search pull-left" id="form_hide" style="display: none;">
                    <input type="text" class="search-query" placeholder="Search">
                </form>

            </div>

        </div>
    </div>
</div>
<%--<br/>--%>
<%--<hr/>--%>
<script type="text/javascript" src="<c:url value='/resources/js/polres/navbar.js'/>"></script>
