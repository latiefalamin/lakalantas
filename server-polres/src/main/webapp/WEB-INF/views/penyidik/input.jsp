<%--
  Created by IntelliJ IDEA.
  User: latief
  Date: 3/20/12
  Time: 11:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="disabledInput" value="${event == 'DETAIL'}"/>
<c:url value="/penyidik" var="penyidik_url"/>


<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>

    <h3>${event} PENYIDIK</h3>
</div>
<form:form id="formInputPenyidik" class="modal-form" action="${penyidik_url}" method="${httpMethod}"
           modelAttribute="penyidik">
    <div class="modal-body">

        <!--agar sejajar antara label dan inputan.-->
        <table>

            <tr>
                <td>
                    <label class="control-label" for="textNrp">NRP</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="nrp" id="textNrp" class="input-xlarge" disabled="${disabledInput}" readonly="true"/>
                        &nbsp;
                        <span class="label label-info">*</span>
                    </div>
                </td>
            </tr>

                <%--Nama--%>
            <tr>
                <td>
                    <label class="control-label" for="textNama">Nama</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="nama" id="textNama" class="input-xlarge" disabled="${disabledInput}"/>
                        &nbsp;
                        <span class="label label-info">*</span>
                    </div>
                </td>
            </tr>

                <%--Pangkat--%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="textPangkat" class="input-xlarge">Pangkat</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:input path="pangkat" id="textPangkat" class="input-xlarge" disabled="${disabledInput}"/>
                        &nbsp;
                        <span class="label label-info">*</span>
                    </div>
                </td>
            </tr>

                <%--polres--%>
            <tr>
                <td valign="top">
                    <label class="control-label" for="selectPolresId" class="input-xlarge">Polres</label>
                </td>
                <td>
                    <div class="controls">
                        &nbsp;&nbsp;
                        <form:select path="polresId" id="selectPolresId" class="input-xlarge" disabled="${disabledInput}">
                            <c:forEach var="polres" items="${polreses}">
                                <form:option value="${polres.id}" label="${polres.id} ${polres.nama}"/>
                            </c:forEach>
                        </form:select>
                        &nbsp;
                        <span class="label label-info">*</span>
                    </div>
                </td>
            </tr>

        </table>

    </div>
    <div class="modal-footer">

            <%--tombol hanya muncul waktu input dan edit--%>
        <c:if test="${!disabledInput}">
            <input type="submit" class="btn btn-primary" value="Simpan"/>
            <input type="reset" class="btn" data-dismiss="modal" value="Batal">
        </c:if>

    </div>
</form:form>

<script type="text/javascript" src="<c:url value='/resources/js/form.js'/>"></script>
