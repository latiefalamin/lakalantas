package com.secondstack.lakalantas.polres.controller.rest;

import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.editor.PolresEditor;
import com.secondstack.lakalantas.core.service.PolresService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "rest/polres")
public class PolresRestController {

    @Autowired
    @Qualifier("polresPolresService")
    private PolresService polresService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find() {
        return polresService.find();
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public
    @ResponseBody
    Polres find(@RequestParam("id") Polres polres) {
        return polres;
    }

    /**
     * Simpan hasil inputan
     *
     * @param polres
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Polres polres) {

        if (polres == null) return "Gagal";

        try {
            polresService.save(polres);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil inputan
     *
     * @param polreses
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public
    @ResponseBody
    String saves(@RequestBody Polres [] polreses) {

        if (polreses == null) return "Gagal";

        try {
            polresService.save(polreses);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Polres polres) {
        try {
            polresService.delete(polres);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param polres
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Polres polres) {

        if (polres == null) return "Gagal";

        try {
            polresService.update(polres);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }
    /**
     * Simpan hasil inputan
     *
     * @param polreses
     */
    @RequestMapping(value = "list", method = RequestMethod.PUT)
    public
    @ResponseBody
    String updates(@RequestBody Polres [] polreses) {

        if (polreses == null) return "Gagal";

        try {
            polresService.save(polreses);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }
    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Polres.class, new PolresEditor(polresService));
    }
}
