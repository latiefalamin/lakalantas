package com.secondstack.lakalantas.polres.controller.rest;

import com.secondstack.lakalantas.core.domain.Kendaraan;
import com.secondstack.lakalantas.core.editor.KendaraanEditor;
import com.secondstack.lakalantas.core.service.KendaraanService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "rest/kendaraan")
public class KendaraanRestController {

    @Autowired
    @Qualifier("kendaraanPolresService")
    private KendaraanService kendaraanService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find(@RequestParam(value = "isSimple", defaultValue = "false")boolean isSimple) {
        if(isSimple){
            return kendaraanService.findSimple();
        }else{
            return kendaraanService.find();
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object find(@RequestParam("id") Kendaraan kendaraan, @RequestParam(value = "isSimple", defaultValue = "false")boolean isSimple) {
        if(isSimple){
            return kendaraan.cloneToSimpleKendaraan();
        }else{
            return kendaraan;
        }
    }

    /**
     * Simpan hasil inputan
     *
     * @param kendaraan
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Kendaraan kendaraan) {

        if (kendaraan == null) return "Gagal";

        try {
            kendaraanService.save(kendaraan);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Kendaraan kendaraan) {
        try {
            kendaraanService.delete(kendaraan);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param kendaraan
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Kendaraan kendaraan) {

        if (kendaraan == null) return "Gagal";

        try {
            kendaraanService.save(kendaraan);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Kendaraan.class, new KendaraanEditor(kendaraanService));
    }
}
