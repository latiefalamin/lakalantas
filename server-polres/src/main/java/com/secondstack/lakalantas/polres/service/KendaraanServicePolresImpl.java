package com.secondstack.lakalantas.polres.service;

import com.secondstack.lakalantas.core.service.impl.KendaraanServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("kendaraanPolresService")
public class KendaraanServicePolresImpl extends KendaraanServiceImpl {
}
