package com.secondstack.lakalantas.polres.controller.rest;

import com.secondstack.lakalantas.core.domain.Penyidik;
import com.secondstack.lakalantas.core.editor.PenyidikEditor;
import com.secondstack.lakalantas.core.service.PenyidikService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "rest/penyidik")
public class PenyidikRestController {

    @Autowired
    @Qualifier("penyidikPolresService")
    private PenyidikService penyidikService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find(@RequestParam(value = "isSimple", defaultValue = "false")boolean isSimple) {
        if(isSimple){
            return penyidikService.findSimple();
        }else{
            return penyidikService.find();
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object find(@RequestParam("id") Penyidik penyidik, @RequestParam(value = "isSimple", defaultValue = "false")boolean isSimple) {
        if(isSimple){
            return penyidik == null ? null : penyidik.cloneToSimplePenyidik();
        }else{
            return penyidik;
        }
    }

    /**
     * Simpan hasil inputan
     *
     * @param penyidik
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Penyidik penyidik) {

        if (penyidik == null) return "Gagal";

        try {
            penyidikService.save(penyidik);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Penyidik penyidik) {
        try {
            penyidikService.delete(penyidik);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param penyidik
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Penyidik penyidik) {

        if (penyidik == null) return "Gagal";

        try {
            penyidikService.save(penyidik);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Penyidik.class, new PenyidikEditor(penyidikService));
    }
}
