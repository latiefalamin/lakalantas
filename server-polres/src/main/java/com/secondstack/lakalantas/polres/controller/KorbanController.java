package com.secondstack.lakalantas.polres.controller;

import com.secondstack.lakalantas.core.domain.Korban;
import com.secondstack.lakalantas.core.domain.enumeration.Gender;
import com.secondstack.lakalantas.core.domain.enumeration.KondisiKorban;
import com.secondstack.lakalantas.core.editor.KorbanEditor;
import com.secondstack.lakalantas.core.service.KorbanService;
import com.secondstack.lakalantas.core.service.LakalantasService;
import com.secondstack.lakalantas.core.simple.SimpleKorban;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "korban")
public class KorbanController {

    @Autowired
    @Qualifier("korbanPolresService")
    private KorbanService korbanService;

    @Autowired
    @Qualifier("lakalantasPolresService")
    private LakalantasService lakalantasService;

    @RequestMapping(value = "navbar", method = RequestMethod.GET)
    public String navBar() {
        return "korban/navbar";
    }

    /**
     * Ini select * all
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("korbans", korbanService.findSimple());
        return "korban/list";
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("id") Korban korban) {

        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("kondisiKorbans", KondisiKorban.values());
        modelMap.addAttribute("genders", Gender.values());
        modelMap.addAttribute("lakalantases", lakalantasService.find());
        modelMap.addAttribute("korban", korban.cloneToSimpleKorban());

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            modelMap.addAttribute("korbanJson", objectMapper.writeValueAsString(korban.cloneToSimpleKorban()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "korban/input";
    }

    /**
     * kembalikan form inputan
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("korban", new SimpleKorban());
        modelMap.addAttribute("kondisiKorbans", KondisiKorban.values());
        modelMap.addAttribute("genders", Gender.values());
        modelMap.addAttribute("lakalantases", lakalantasService.find());

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            modelMap.addAttribute("korbanJson", objectMapper.writeValueAsString(new SimpleKorban()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "korban/input";
    }

    /**
     * Simpan hasil inputan
     *
     * @param korban
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody SimpleKorban korban) {

        if (korban == null) return "Gagal";

        try {
            korbanService.save(korban.cloneToKorban());
            return "Sukses";
        } catch (HibernateException e) {
            e.printStackTrace();
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Korban korban) {
        try {
            korbanService.delete(korban);
            return "Sukses";
        } catch (HibernateException e) {
            e.printStackTrace();
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     *
     * @param modelMap
     * @param korban
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id") Korban korban) {
        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("korban", korban.cloneToSimpleKorban());
        modelMap.addAttribute("kondisiKorbans", KondisiKorban.values());
        modelMap.addAttribute("genders", Gender.values());
        modelMap.addAttribute("lakalantases", lakalantasService.find());

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            modelMap.addAttribute("korbanJson", objectMapper.writeValueAsString(korban.cloneToSimpleKorban()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "korban/input";
    }

    /**
     * Simpan hasil editan
     *
     * @param korban
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody SimpleKorban korban) {

        if (korban == null) return "Gagal";

        try {
            korbanService.save(korban.cloneToKorban());
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Korban.class, new KorbanEditor(korbanService));
    }
}
