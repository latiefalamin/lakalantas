package com.secondstack.lakalantas.polres.controller.rest;

import com.secondstack.lakalantas.core.domain.RumahSakit;
import com.secondstack.lakalantas.core.editor.RumahSakitEditor;
import com.secondstack.lakalantas.core.service.RumahSakitService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "rest/rumahSakit")
public class RumahSakitRestController {

    @Autowired
    @Qualifier("rumahSakitPolresService")
    private RumahSakitService rumahSakitService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find() {
        return rumahSakitService.find();
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public
    @ResponseBody
    RumahSakit find(@RequestParam("id") RumahSakit rumahSakit) {
        return rumahSakit;
    }

    /**
     * Simpan hasil inputan
     *
     * @param rumahSakit
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody RumahSakit rumahSakit) {

        if (rumahSakit == null) return "Gagal";

        try {
            rumahSakitService.save(rumahSakit);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil inputan
     *
     * @param rumahSakites
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public
    @ResponseBody
    String saves(@RequestBody RumahSakit [] rumahSakites) {

        if (rumahSakites == null) return "Gagal";

        try {
            rumahSakitService.save(rumahSakites);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") RumahSakit rumahSakit) {
        try {
            rumahSakitService.delete(rumahSakit);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param rumahSakit
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody RumahSakit rumahSakit) {

        if (rumahSakit == null) return "Gagal";

        try {
            rumahSakitService.save(rumahSakit);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }
    /**
     * Simpan hasil inputan
     *
     * @param rumahSakites
     */
    @RequestMapping(value = "list", method = RequestMethod.PUT)
    public
    @ResponseBody
    String updates(@RequestBody RumahSakit [] rumahSakites) {

        if (rumahSakites == null) return "Gagal";

        try {
            rumahSakitService.save(rumahSakites);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }
    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(RumahSakit.class, new RumahSakitEditor(rumahSakitService));
    }
}
