package com.secondstack.lakalantas.polres.controller.rest;

import com.secondstack.lakalantas.core.domain.Santunan;
import com.secondstack.lakalantas.core.editor.SantunanEditor;
import com.secondstack.lakalantas.core.service.SantunanService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "rest/santunan")
public class SantunanRestController {

    @Autowired
    @Qualifier("santunanPolresService")
    private SantunanService santunanService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find() {
        return santunanService.find();
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object find(@RequestParam("id") Santunan santunan) {
        return santunan;
    }

    /**
     * Simpan hasil inputan
     *
     * @param santunan
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Santunan santunan) {

        if (santunan == null) return "Gagal";

        try {
            santunanService.save(santunan);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Santunan santunan) {
        try {
            santunanService.delete(santunan);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param santunan
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Santunan santunan) {

        if (santunan == null) return "Gagal";

        try {
            santunanService.save(santunan);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Santunan.class, new SantunanEditor(santunanService));
    }
}
