package com.secondstack.lakalantas.polres.controller.rest;

import com.secondstack.lakalantas.core.domain.Lakalantas;
import com.secondstack.lakalantas.core.editor.LakalantasEditor;
import com.secondstack.lakalantas.core.service.LakalantasService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "rest/lakalantas")
public class LakalantasRestController {

    @Autowired
    @Qualifier("lakalantasPolresService")
    private LakalantasService lakalantasService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find(@RequestParam(value = "isSimple", defaultValue = "false")boolean isSimple) {
        if(isSimple){
            return lakalantasService.findSimple();
        }else{
            return lakalantasService.find();
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object find(@RequestParam("id") Lakalantas lakalantas, @RequestParam(value = "isSimple", defaultValue = "false")boolean isSimple) {
        if(isSimple){
            return lakalantas == null ? null : lakalantas.cloneToSimpleLakalantas();
        }else{
            return lakalantas;
        }
    }

    /**
     * Simpan hasil inputan
     *
     * @param lakalantas
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Lakalantas lakalantas) {

        if (lakalantas == null) return "Gagal";

        try {
            lakalantasService.save(lakalantas);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Lakalantas lakalantas) {
        try {
            lakalantasService.delete(lakalantas);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param lakalantas
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Lakalantas lakalantas) {

        if (lakalantas == null) return "Gagal";

        try {
            lakalantasService.save(lakalantas);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Lakalantas.class, new LakalantasEditor(lakalantasService));
    }
}
