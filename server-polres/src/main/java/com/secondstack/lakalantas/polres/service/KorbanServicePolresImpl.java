package com.secondstack.lakalantas.polres.service;

import com.secondstack.lakalantas.core.domain.Korban;
import com.secondstack.lakalantas.core.domain.Rawat;
import com.secondstack.lakalantas.core.service.RawatService;
import com.secondstack.lakalantas.core.service.SantunanService;
import com.secondstack.lakalantas.core.service.impl.KorbanServiceImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("korbanPolresService")
public class KorbanServicePolresImpl extends KorbanServiceImpl {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    @Qualifier("rawatPolresService")
    private RawatService rawatService;

    @Autowired
    @Qualifier("santunanPolresService")
    private SantunanService santunanService;

    @Override
    public void save(Korban korban) {
        if (korban == null) throw new IllegalArgumentException("Cannot save null value of " + Korban.class);

        santunanService.save(korban.getSantunans());
        rawatService.save(korban.getRawats());

        korban.setId(korban.getId() == null ? generateNextId() : korban.getId().trim());
        sessionFactory.getCurrentSession().saveOrUpdate(korban);
    }
}
