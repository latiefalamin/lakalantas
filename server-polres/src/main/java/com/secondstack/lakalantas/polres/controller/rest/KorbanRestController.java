package com.secondstack.lakalantas.polres.controller.rest;

import com.secondstack.lakalantas.core.domain.Korban;
import com.secondstack.lakalantas.core.editor.KorbanEditor;
import com.secondstack.lakalantas.core.service.KorbanService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "rest/korban")
public class KorbanRestController {

    @Autowired
    @Qualifier("korbanPolresService")
    private KorbanService korbanService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find(@RequestParam(value = "isSimple", defaultValue = "false")boolean isSimple) {
        if(isSimple){
            return korbanService.findSimple();
        }else{
            return korbanService.find();
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object find(@RequestParam(value = "isSimple", defaultValue = "false")boolean isSimple, @RequestParam("id") Korban korban) {
        if(isSimple){
            return korban.cloneToSimpleKorban();
        }
        return korban;
    }

    /**
     * Simpan hasil inputan
     *
     * @param korban
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Korban korban) {

        if (korban == null) return "Gagal";

        try {
            korbanService.save(korban);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Korban korban) {
        try {
            korbanService.delete(korban);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param korban
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Korban korban) {

        if (korban == null) return "Gagal";

        try {
            korbanService.save(korban);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Korban.class, new KorbanEditor(korbanService));
    }
}
