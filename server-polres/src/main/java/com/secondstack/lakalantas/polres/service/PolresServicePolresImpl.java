package com.secondstack.lakalantas.polres.service;

import com.secondstack.lakalantas.core.service.impl.PolresServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/28/12
 * Time: 12:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
@Service("polresPolresService")
public class PolresServicePolresImpl extends PolresServiceImpl{
}
