package com.secondstack.lakalantas.polres.controller.rest;

import com.secondstack.lakalantas.core.domain.Asuransi;
import com.secondstack.lakalantas.core.editor.AsuransiEditor;
import com.secondstack.lakalantas.core.service.AsuransiService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "rest/asuransi")
public class AsuransiRestController {

    @Autowired
    @Qualifier("asuransiPolresService")
    private AsuransiService asuransiService;

    /**
     * Ini select * all
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List find() {
        return asuransiService.find();
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public
    @ResponseBody
    Asuransi find(@RequestParam("id") Asuransi asuransi) {
        return asuransi;
    }

    /**
     * Simpan hasil inputan
     *
     * @param asuransi
     */
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestBody Asuransi asuransi) {

        if (asuransi == null) return "Gagal";

        try {
            asuransiService.save(asuransi);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil inputan
     *
     * @param asuransies
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public
    @ResponseBody
    String saves(@RequestBody Asuransi [] asuransies) {

        if (asuransies == null) return "Gagal";

        try {
            asuransiService.save(asuransies);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam("id") Asuransi asuransi) {
        try {
            asuransiService.delete(asuransi);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }

    /**
     * Simpan hasil editan
     *
     * @param asuransi
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public
    @ResponseBody
    String update(@RequestBody Asuransi asuransi) {

        if (asuransi == null) return "Gagal";

        try {
            asuransiService.save(asuransi);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }
    /**
     * Simpan hasil inputan
     *
     * @param asuransies
     */
    @RequestMapping(value = "list", method = RequestMethod.PUT)
    public
    @ResponseBody
    String updates(@RequestBody Asuransi [] asuransies) {

        if (asuransies == null) return "Gagal";

        try {
            asuransiService.save(asuransies);
            return "Sukses";
        } catch (HibernateException e) {
            return "Gagal";
        }
    }
    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Asuransi.class, new AsuransiEditor(asuransiService));
    }
}
