package com.secondstack.lakalantas.polres.controller;

import com.secondstack.lakalantas.core.domain.Kendaraan;
import com.secondstack.lakalantas.core.domain.enumeration.JenisKendaraan;
import com.secondstack.lakalantas.core.editor.KendaraanEditor;
import com.secondstack.lakalantas.core.service.KendaraanService;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.simple.SimpleKendaraan;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/19/12
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "kendaraan")
public class KendaraanController {

    @Autowired
    @Qualifier("kendaraanPolresService")
    private KendaraanService kendaraanService;

    @Autowired
    @Qualifier("polresPolresService")
    private PolresService polresService;

    @RequestMapping(value = "navbar", method = RequestMethod.GET)
    public String navBar() {
        return "kendaraan/navbar";
    }

    /**
     * Ini select * all
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap) {
        modelMap.addAttribute("kendaraans", kendaraanService.find());
        return "kendaraan/list";
    }

    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String find(ModelMap modelMap, @RequestParam("id") Kendaraan kendaraan) {
        modelMap.addAttribute("event", "DETAIL");
        modelMap.addAttribute("polreses", polresService.find());
        modelMap.addAttribute("jenisKendaraans", JenisKendaraan.values());
        modelMap.addAttribute("kendaraan", kendaraan.cloneToSimpleKendaraan());
        return "kendaraan/input";
    }

    /**
     * kembalikan form inputan
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("event", "INPUT");
        modelMap.addAttribute("httpMethod", "POST");
        modelMap.addAttribute("polreses", polresService.find());
        modelMap.addAttribute("jenisKendaraans", JenisKendaraan.values());
        modelMap.addAttribute("kendaraan", new SimpleKendaraan());
        return "kendaraan/input";
    }

    /***
     * Simpan hasil inputan
     * @param kendaraan
     * @param bindingResult
     * @param modelMap
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String save(@Valid @ModelAttribute("kendaraan") SimpleKendaraan kendaraan, BindingResult bindingResult, ModelMap modelMap){

        if(kendaraan == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
           kendaraanService.save(kendaraan);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    @RequestMapping(params = "id", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@RequestParam("id") Kendaraan kendaraan) {
        try {
            kendaraanService.delete(kendaraan);
            return "Sukses";
        }catch (HibernateException e){
            return "Gagal";
        }
    }

    /**
     * kembalikan form edit
     * @param modelMap
     * @param kendaraan
     * @return
     */
    @RequestMapping(value = "edit", params = "id", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @RequestParam("id")Kendaraan kendaraan) {

        modelMap.addAttribute("event", "EDIT");
        modelMap.addAttribute("httpMethod", "PUT");
        modelMap.addAttribute("polreses", polresService.find());
        modelMap.addAttribute("jenisKendaraans", JenisKendaraan.values());
        modelMap.addAttribute("kendaraan", kendaraan.cloneToSimpleKendaraan());
        return "kendaraan/input";
    }

    /**
     * Simpan hasil editan
     * @param kendaraan
     * @param bindingResult
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String update(@Valid @ModelAttribute("kendaraan") SimpleKendaraan kendaraan, BindingResult bindingResult, ModelMap modelMap){

        if(kendaraan == null) return "Gagal";
        if(bindingResult.hasErrors()) return "Gagal";

        try{
            kendaraanService.save(kendaraan);
            return "Sukses";
        }catch (HibernateException e){
            e.printStackTrace();
            return "Gagal";
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(Kendaraan.class, new KendaraanEditor(kendaraanService));
    }
}
