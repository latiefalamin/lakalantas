package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.Lakalantas;
import com.secondstack.lakalantas.core.simple.SimpleLakalantas;

import java.util.Calendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface LakalantasService {
    
    public void save(Lakalantas lakalantas);
    public void save(SimpleLakalantas simpleLakalantas);
    public void delete(Lakalantas lakalantas);
    public void deletes(Lakalantas[] lakalantass);
    public Lakalantas find(String id);
    public SimpleLakalantas findSimple(String id);
    public List<Lakalantas> find();
    public List<SimpleLakalantas> findSimple();
    public Long count();
    public String generateNextId(String polresId, Calendar waktuKejadian);
}
