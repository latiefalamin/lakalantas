package com.secondstack.lakalantas.core.simple;

import com.secondstack.lakalantas.core.domain.Kendaraan;
import com.secondstack.lakalantas.core.domain.Lakalantas;
import com.secondstack.lakalantas.core.domain.Penyidik;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.domain.enumeration.JenisLakalantas;
import com.secondstack.lakalantas.core.domain.enumeration.KondisiKendaraan;
import com.secondstack.lakalantas.core.util.CalendarUtils;

import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 4/14/12
 * Time: 4:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleLakalantas {
    
    private String id;
    private String nopol;
    private String polresKendaraanId;
    private String polresKendaraanNama;
    private String penyidikNrp;
    private String penyidikNama;
    private String polresPenyidikId;
    private String polresPenyidikNama;
    private JenisLakalantas jenisLakalantas;
    private KondisiKendaraan kondisiKendaraan;
    private String waktuKejadian;
    private String tempatKejadian;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNopol() {
        return nopol;
    }

    public void setNopol(String nopol) {
        this.nopol = nopol;
    }

    public String getPolresKendaraanId() {
        return polresKendaraanId;
    }

    public void setPolresKendaraanId(String polresKendaraanId) {
        this.polresKendaraanId = polresKendaraanId;
    }

    public String getPolresKendaraanNama() {
        return polresKendaraanNama;
    }

    public void setPolresKendaraanNama(String polresKendaraanNama) {
        this.polresKendaraanNama = polresKendaraanNama;
    }

    public String getPenyidikNrp() {
        return penyidikNrp;
    }

    public void setPenyidikNrp(String penyidikNrp) {
        this.penyidikNrp = penyidikNrp;
    }

    public String getPenyidikNama() {
        return penyidikNama;
    }

    public void setPenyidikNama(String penyidikNama) {
        this.penyidikNama = penyidikNama;
    }

    public String getPolresPenyidikId() {
        return polresPenyidikId;
    }

    public void setPolresPenyidikId(String polresPenyidikId) {
        this.polresPenyidikId = polresPenyidikId;
    }

    public String getPolresPenyidikNama() {
        return polresPenyidikNama;
    }

    public void setPolresPenyidikNama(String polresPenyidikNama) {
        this.polresPenyidikNama = polresPenyidikNama;
    }

    public JenisLakalantas getJenisLakalantas() {
        return jenisLakalantas;
    }

    public void setJenisLakalantas(JenisLakalantas jenisLakalantas) {
        this.jenisLakalantas = jenisLakalantas;
    }

    public KondisiKendaraan getKondisiKendaraan() {
        return kondisiKendaraan;
    }

    public void setKondisiKendaraan(KondisiKendaraan kondisiKendaraan) {
        this.kondisiKendaraan = kondisiKendaraan;
    }

    public String getWaktuKejadian() {
        return waktuKejadian;
    }

    public void setWaktuKejadian(String waktuKejadian) {
        this.waktuKejadian = waktuKejadian;
    }

    public String getTempatKejadian() {
        return tempatKejadian;
    }

    public void setTempatKejadian(String tempatKejadian) {
        this.tempatKejadian = tempatKejadian;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleLakalantas)) return false;

        SimpleLakalantas that = (SimpleLakalantas) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (jenisLakalantas != that.jenisLakalantas) return false;
        if (kondisiKendaraan != that.kondisiKendaraan) return false;
        if (nopol != null ? !nopol.equals(that.nopol) : that.nopol != null) return false;
        if (penyidikNama != null ? !penyidikNama.equals(that.penyidikNama) : that.penyidikNama != null) return false;
        if (penyidikNrp != null ? !penyidikNrp.equals(that.penyidikNrp) : that.penyidikNrp != null) return false;
        if (polresKendaraanId != null ? !polresKendaraanId.equals(that.polresKendaraanId) : that.polresKendaraanId != null)
            return false;
        if (polresKendaraanNama != null ? !polresKendaraanNama.equals(that.polresKendaraanNama) : that.polresKendaraanNama != null)
            return false;
        if (polresPenyidikId != null ? !polresPenyidikId.equals(that.polresPenyidikId) : that.polresPenyidikId != null)
            return false;
        if (polresPenyidikNama != null ? !polresPenyidikNama.equals(that.polresPenyidikNama) : that.polresPenyidikNama != null)
            return false;
        if (tempatKejadian != null ? !tempatKejadian.equals(that.tempatKejadian) : that.tempatKejadian != null)
            return false;
        if (waktuKejadian != null ? !waktuKejadian.equals(that.waktuKejadian) : that.waktuKejadian != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nopol != null ? nopol.hashCode() : 0);
        result = 31 * result + (polresKendaraanId != null ? polresKendaraanId.hashCode() : 0);
        result = 31 * result + (polresKendaraanNama != null ? polresKendaraanNama.hashCode() : 0);
        result = 31 * result + (penyidikNrp != null ? penyidikNrp.hashCode() : 0);
        result = 31 * result + (penyidikNama != null ? penyidikNama.hashCode() : 0);
        result = 31 * result + (polresPenyidikId != null ? polresPenyidikId.hashCode() : 0);
        result = 31 * result + (polresPenyidikNama != null ? polresPenyidikNama.hashCode() : 0);
        result = 31 * result + (jenisLakalantas != null ? jenisLakalantas.hashCode() : 0);
        result = 31 * result + (kondisiKendaraan != null ? kondisiKendaraan.hashCode() : 0);
        result = 31 * result + (waktuKejadian != null ? waktuKejadian.hashCode() : 0);
        result = 31 * result + (tempatKejadian != null ? tempatKejadian.hashCode() : 0);
        return result;
    }

    public Lakalantas cloneToLakalantas(){

        Polres polresKendaraan = new Polres();
        polresKendaraan.setId(polresKendaraanId);
        polresKendaraan.setNama(polresKendaraanNama);

        Polres polresPenyidik = new Polres();
        polresPenyidik.setId(polresPenyidikId);
        polresPenyidik.setNama(polresPenyidikNama);

        Kendaraan kendaraan = new Kendaraan();
        kendaraan.setNopol(nopol);
        kendaraan.setPolres(polresKendaraan);

        Penyidik penyidik = new Penyidik();
        penyidik.setNrp(penyidikNrp);
        penyidik.setNama(penyidikNama);
        penyidik.setPolres(polresPenyidik);

        Lakalantas lakalantas = new Lakalantas();
        lakalantas.setId(id);
        lakalantas.setJenisLakalantas(jenisLakalantas);
        lakalantas.setKondisiKendaraan(kondisiKendaraan);
        lakalantas.setTempatKejadian(tempatKejadian);
        lakalantas.setKendaraan(kendaraan);
        lakalantas.setPenyidik(penyidik);
        lakalantas.setWaktuKejadian(CalendarUtils.stringToTime(waktuKejadian));

        return lakalantas;
    }
}
