package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.RumahSakit;
import com.secondstack.lakalantas.core.service.RumahSakitService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class RumahSakitEditor extends PropertyEditorSupport {

    private RumahSakitService rumahSakitService;

    public RumahSakitEditor(RumahSakitService rumahSakitService) {
        this.rumahSakitService = rumahSakitService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        RumahSakit rumahSakit = rumahSakitService.find(text);

        if (rumahSakit != null) {
            setValue(rumahSakit);
        } else {
            throw new IllegalArgumentException("RumahSakit with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            RumahSakit rumahSakit = (RumahSakit) getValue();
            return rumahSakit.getId() + "";
        } else {
            return "";
        }
    }
}
