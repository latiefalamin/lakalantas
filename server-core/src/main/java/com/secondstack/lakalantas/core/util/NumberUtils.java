package com.secondstack.lakalantas.core.util;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 4/10/12
 * Time: 9:51 PM
 * To change this template use File | Settings | File Templates.
 */
public final class NumberUtils {

    /**
     * rubah number ke string. dengan menggenapkan digitnya.ditambahkan 0 didepan agar genap digitnya.
     * 10 -> '00010'
     * @param number
     * @param digit
     * @return
     */
    public final static String toString(int number, int digit) {
        if (digit <= 0)
            return null;

        String numberString = number < 0 ? (number * -1) + "" : number + "";
        for (int i = numberString.length(); i < digit; i++) {
            numberString = "0" + numberString;
        }

        return number < 0 ? "-" + numberString : numberString;
    }

}
