package com.secondstack.lakalantas.core.domain;

import com.secondstack.lakalantas.core.simple.SimpleRawat;
import com.secondstack.lakalantas.core.util.CalendarUtils;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:31 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "rawat")
public class Rawat {

    /**
     * 17 digit awal adalah id korban
     * 2 figit akhir adalah id rumah sakit
     */
    @Id
    @Column(name = "rawat_id", length = 19)
    private String id;

    @ManyToOne
    @JoinColumn(name = "rumah_sakit_id", nullable = false)
    private RumahSakit rumahSakit;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "waktu_masuk", nullable = false)
    private Calendar waktuMasuk;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "waktu_keluar", nullable = true)
    private Calendar waktuKeluar;

    @Column(name = "biaya", nullable = false)
    private Float biaya;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RumahSakit getRumahSakit() {
        return rumahSakit;
    }

    public void setRumahSakit(RumahSakit rumahSakit) {
        this.rumahSakit = rumahSakit;
    }

    public Calendar getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(Calendar waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public Calendar getWaktuKeluar() {
        return waktuKeluar;
    }

    public void setWaktuKeluar(Calendar waktuKeluar) {
        this.waktuKeluar = waktuKeluar;
    }

    public Float getBiaya() {
        return biaya;
    }

    public void setBiaya(Float biaya) {
        this.biaya = biaya;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rawat)) return false;

        Rawat rawat = (Rawat) o;

        if (biaya != null ? !biaya.equals(rawat.biaya) : rawat.biaya != null) return false;
        if (id != null ? !id.equals(rawat.id) : rawat.id != null) return false;
        if (rumahSakit != null ? !rumahSakit.equals(rawat.rumahSakit) : rawat.rumahSakit != null) return false;
        if (waktuKeluar != null ? !waktuKeluar.equals(rawat.waktuKeluar) : rawat.waktuKeluar != null) return false;
        if (waktuMasuk != null ? !waktuMasuk.equals(rawat.waktuMasuk) : rawat.waktuMasuk != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (rumahSakit != null ? rumahSakit.hashCode() : 0);
        result = 31 * result + (waktuMasuk != null ? waktuMasuk.hashCode() : 0);
        result = 31 * result + (waktuKeluar != null ? waktuKeluar.hashCode() : 0);
        result = 31 * result + (biaya != null ? biaya.hashCode() : 0);
        return result;
    }

    public SimpleRawat cloneToSimpleRawat(){
        SimpleRawat simpleRawat = new SimpleRawat();
        simpleRawat.setBiaya(biaya);
        simpleRawat.setId(id);
        simpleRawat.setRumahSakit(rumahSakit);
        simpleRawat.setWaktuKeluar(CalendarUtils.timeToString(waktuKeluar));
        simpleRawat.setWaktuMasuk(CalendarUtils.timeToString(waktuMasuk));
        return simpleRawat;
    }
}
