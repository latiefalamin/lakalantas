package com.secondstack.lakalantas.core.domain;

import com.secondstack.lakalantas.core.domain.enumeration.Gender;
import com.secondstack.lakalantas.core.domain.enumeration.KondisiKorban;
import com.secondstack.lakalantas.core.simple.SimpleKorban;
import com.secondstack.lakalantas.core.simple.SimpleRawat;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:30 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "korban")
public class Korban {

    /**
     * 15 digit awal adalah id lakalantas - lihat lakalantas -
     * 2 digit akhir no urut kecelakaan - urut sesuai input -
     */
    @Id
    @Column(name = "korban_id", length = 17)
    private String id;

    @ManyToOne
    @JoinColumn(name = "lakalantas_id", nullable = false)
    private Lakalantas lakalantas;

    @Column(name = "nik", nullable = false, length = 16)
    private String nik;

    @Column(name = "nama", nullable = false, length = 50)
    private String nama;

    @Column(name = "umur", nullable = false)
    private int umur;

    @Column(name = "alamat", nullable = false)
    private String alamat;

    @Column(name = "kondisi_korban", nullable = false)
    private KondisiKorban kondisiKorban;

    @Column(name = "gender", nullable = false)
    private Gender gender;

    @OneToMany
    @JoinTable(
            name = "rawat_list",
            joinColumns = {@JoinColumn(name = "korban_id")},
            inverseJoinColumns = {@JoinColumn(table = "rawat", name = "rawat_id")}
    )
    private List<Rawat> rawats;

    @OneToMany
    @JoinTable(
            name = "santunan_list",
            joinColumns = {@JoinColumn(name = "korban_id")},
            inverseJoinColumns = {@JoinColumn(table = "santunan", name = "santunan_id")}
    )
    private List<Santunan> santunans;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Lakalantas getLakalantas() {
        return lakalantas;
    }

    public void setLakalantas(Lakalantas lakalantas) {
        this.lakalantas = lakalantas;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public KondisiKorban getKondisiKorban() {
        return kondisiKorban;
    }

    public void setKondisiKorban(KondisiKorban kondisiKorban) {
        this.kondisiKorban = kondisiKorban;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<Rawat> getRawats() {
        return rawats;
    }

    public void setRawats(List<Rawat> rawats) {
        this.rawats = rawats;
    }

    public List<Santunan> getSantunans() {
        return santunans;
    }

    public void setSantunans(List<Santunan> santunans) {
        this.santunans = santunans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Korban)) return false;

        Korban korban = (Korban) o;

        if (umur != korban.umur) return false;
        if (alamat != null ? !alamat.equals(korban.alamat) : korban.alamat != null) return false;
        if (gender != korban.gender) return false;
        if (id != null ? !id.equals(korban.id) : korban.id != null) return false;
        if (kondisiKorban != korban.kondisiKorban) return false;
        if (lakalantas != null ? !lakalantas.equals(korban.lakalantas) : korban.lakalantas != null) return false;
        if (nama != null ? !nama.equals(korban.nama) : korban.nama != null) return false;
        if (nik != null ? !nik.equals(korban.nik) : korban.nik != null) return false;
        if (rawats != null ? !rawats.equals(korban.rawats) : korban.rawats != null) return false;
        if (santunans != null ? !santunans.equals(korban.santunans) : korban.santunans != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (lakalantas != null ? lakalantas.hashCode() : 0);
        result = 31 * result + (nik != null ? nik.hashCode() : 0);
        result = 31 * result + (nama != null ? nama.hashCode() : 0);
        result = 31 * result + umur;
        result = 31 * result + (alamat != null ? alamat.hashCode() : 0);
        result = 31 * result + (kondisiKorban != null ? kondisiKorban.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (rawats != null ? rawats.hashCode() : 0);
        result = 31 * result + (santunans != null ? santunans.hashCode() : 0);
        return result;
    }

    public SimpleKorban cloneToSimpleKorban(){
        SimpleKorban simpleKorban = new SimpleKorban();
        simpleKorban.setAlamat(alamat);
        simpleKorban.setGender(gender);
        simpleKorban.setId(id);
        simpleKorban.setKondisiKorban(kondisiKorban);
        simpleKorban.setLakalantas(lakalantas.cloneToSimpleLakalantas());
        simpleKorban.setNama(nama);
        simpleKorban.setNik(nik);
        simpleKorban.setSantunans(santunans);
        simpleKorban.setUmur(umur);
        List<SimpleRawat> rawatList = new ArrayList<SimpleRawat>();
        for(int i = 0;rawats != null && i <rawats.size();i++){
            rawatList.add(rawats.get(i).cloneToSimpleRawat());
        }
        simpleKorban.setRawats(rawatList);
        return simpleKorban;
    }
}
