package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.RumahSakit;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface RumahSakitService {
    
    public void save(RumahSakit rumahSakit);
    public void save(RumahSakit [] rumahSakites);
    public void delete(RumahSakit rumahSakit);
    public void deletes(RumahSakit[] rumahSakits);
    public RumahSakit find(String id);
    public List<RumahSakit> find();
    public Long count();
    public String generateNextId();
}
