package com.secondstack.lakalantas.core.domain.enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:20 AM
 * To change this template use File | Settings | File Templates.
 */
public enum JenisKendaraan {
    SEPEDA_MOTOR, MOBIL_PENUMPANG, MOBIL_BEBAN, BUS;
}
