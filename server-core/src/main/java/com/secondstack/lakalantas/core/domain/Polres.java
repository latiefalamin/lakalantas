package com.secondstack.lakalantas.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:28 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table (name = "polres")
public class Polres {

    /**
     *
     */
    @Id
    @Column(name = "polres_id", length = 3)
    private String id;

    @Column(name = "nama", nullable = false, length = 50)
    private String nama;

    @Column(name = "alamat", nullable = false)
    private String alamat;

    @Column(name = "host", nullable = false, length = 20)
    private String host;

    @Column(name = "port", nullable = false)
    private int port;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Polres)) return false;

        Polres polres = (Polres) o;

        if (port != polres.port) return false;
        if (alamat != null ? !alamat.equals(polres.alamat) : polres.alamat != null) return false;
        if (host != null ? !host.equals(polres.host) : polres.host != null) return false;
        if (id != null ? !id.equals(polres.id) : polres.id != null) return false;
        if (nama != null ? !nama.equals(polres.nama) : polres.nama != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nama != null ? nama.hashCode() : 0);
        result = 31 * result + (alamat != null ? alamat.hashCode() : 0);
        result = 31 * result + (host != null ? host.hashCode() : 0);
        result = 31 * result + port;
        return result;
    }
}
