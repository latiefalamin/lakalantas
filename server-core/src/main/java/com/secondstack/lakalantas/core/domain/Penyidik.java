package com.secondstack.lakalantas.core.domain;

import com.secondstack.lakalantas.core.simple.SimplePenyidik;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "penyidik")
public class Penyidik {

    @Id
    @Column(name = "nrp", length = 5)
    private String nrp;

    @ManyToOne
    @JoinColumn(name = "polres_id", nullable = false)
    private Polres polres;

    @Column(name = "nama", nullable = false, length = 50)
    private String nama;

    @Column(name = "pangkat", nullable = false, length = 50)
    private String pangkat;

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public Polres getPolres() {
        return polres;
    }

    public void setPolres(Polres polres) {
        this.polres = polres;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Penyidik)) return false;

        Penyidik penyidik = (Penyidik) o;

        if (nama != null ? !nama.equals(penyidik.nama) : penyidik.nama != null) return false;
        if (nrp != null ? !nrp.equals(penyidik.nrp) : penyidik.nrp != null) return false;
        if (pangkat != null ? !pangkat.equals(penyidik.pangkat) : penyidik.pangkat != null) return false;
        if (polres != null ? !polres.equals(penyidik.polres) : penyidik.polres != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nrp != null ? nrp.hashCode() : 0;
        result = 31 * result + (polres != null ? polres.hashCode() : 0);
        result = 31 * result + (nama != null ? nama.hashCode() : 0);
        result = 31 * result + (pangkat != null ? pangkat.hashCode() : 0);
        return result;
    }


    public SimplePenyidik cloneToSimplePenyidik(){
        SimplePenyidik penyidik = new SimplePenyidik();
        penyidik.setNama(nama);
        penyidik.setNrp(nrp);
        penyidik.setPangkat(pangkat);
        penyidik.setPolresId(polres == null ? null : polres.getId());
        penyidik.setPolresNama(polres == null ? null : polres.getNama());

        return penyidik;
    }
}
