package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Santunan;
import com.secondstack.lakalantas.core.service.SantunanService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("santunanService")
public class SantunanServiceImpl implements SantunanService {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void save(Santunan santunan) {
        if(santunan == null) throw new IllegalArgumentException("Cannot save null value of" + Santunan.class);
        santunan.setId(santunan.getId() == null ? generateNextId() : santunan.getId().trim());
        sessionFactory.getCurrentSession().saveOrUpdate(santunan);
    }

    @Override
    public void save(List<Santunan> santunans) {
        if (santunans == null) return;
        for(Santunan santunan:santunans){
            save(santunan);
        }
    }

    @Override
    public void delete(Santunan santunan) {
        sessionFactory.getCurrentSession().delete(santunan);
    }

    @Override
    public void deletes(Santunan[] santunans) {
        if(santunans == null) return;
        for(Santunan santunan:santunans){
            delete(santunan);
        }
    }

    @Override
    public Santunan find(String id) {
        return (Santunan) sessionFactory.getCurrentSession().get(Santunan.class, id);
    }

    @Override
    public List<Santunan> find() {
        return (List<Santunan>) sessionFactory.getCurrentSession().createQuery(
                "SELECT santunan from Santunan santunan order by santunan.id")
                .list();
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(santunan) from Santunan santunan ")
                .uniqueResult();
    }

    @Override
    public String generateNextId() {
        return new GregorianCalendar().getTimeInMillis() + "";
    }
}
