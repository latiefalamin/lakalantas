package com.secondstack.lakalantas.core.simple;

import com.secondstack.lakalantas.core.domain.Korban;
import com.secondstack.lakalantas.core.domain.Lakalantas;
import com.secondstack.lakalantas.core.domain.Rawat;
import com.secondstack.lakalantas.core.domain.Santunan;
import com.secondstack.lakalantas.core.domain.enumeration.Gender;
import com.secondstack.lakalantas.core.domain.enumeration.KondisiKorban;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 4/20/12
 * Time: 8:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleKorban {
    private String id;
    private SimpleLakalantas lakalantas;
    private String nik;
    private String nama;
    private int umur;
    private String alamat;
    private KondisiKorban kondisiKorban;
    private Gender gender;
    private List<SimpleRawat> rawats;
    private List<Santunan> santunans;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SimpleLakalantas getLakalantas() {
        return lakalantas;
    }

    public void setLakalantas(SimpleLakalantas lakalantas) {
        this.lakalantas = lakalantas;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public KondisiKorban getKondisiKorban() {
        return kondisiKorban;
    }

    public void setKondisiKorban(KondisiKorban kondisiKorban) {
        this.kondisiKorban = kondisiKorban;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<SimpleRawat> getRawats() {
        return rawats;
    }

    public void setRawats(List<SimpleRawat> rawats) {
        this.rawats = rawats;
    }

    public List<Santunan> getSantunans() {
        return santunans;
    }

    public void setSantunans(List<Santunan> santunans) {
        this.santunans = santunans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleKorban)) return false;

        SimpleKorban that = (SimpleKorban) o;

        if (umur != that.umur) return false;
        if (alamat != null ? !alamat.equals(that.alamat) : that.alamat != null) return false;
        if (gender != that.gender) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (kondisiKorban != that.kondisiKorban) return false;
        if (lakalantas != null ? !lakalantas.equals(that.lakalantas) : that.lakalantas != null) return false;
        if (nama != null ? !nama.equals(that.nama) : that.nama != null) return false;
        if (nik != null ? !nik.equals(that.nik) : that.nik != null) return false;
        if (rawats != null ? !rawats.equals(that.rawats) : that.rawats != null) return false;
        if (santunans != null ? !santunans.equals(that.santunans) : that.santunans != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (lakalantas != null ? lakalantas.hashCode() : 0);
        result = 31 * result + (nik != null ? nik.hashCode() : 0);
        result = 31 * result + (nama != null ? nama.hashCode() : 0);
        result = 31 * result + umur;
        result = 31 * result + (alamat != null ? alamat.hashCode() : 0);
        result = 31 * result + (kondisiKorban != null ? kondisiKorban.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (rawats != null ? rawats.hashCode() : 0);
        result = 31 * result + (santunans != null ? santunans.hashCode() : 0);
        return result;
    }
    
    public Korban cloneToKorban(){
        Korban korban = new Korban();
        korban.setAlamat(alamat);
        korban.setGender(gender);
        korban.setId(id);
        korban.setKondisiKorban(kondisiKorban);
        korban.setLakalantas(lakalantas.cloneToLakalantas());
        korban.setNama(nama);
        korban.setNik(nik);
        korban.setSantunans(santunans);
        korban.setUmur(umur);
        List<Rawat> rawatList = new ArrayList<Rawat>();
        for(int i = 0;rawats != null && i <rawats.size();i++){
            rawatList.add(rawats.get(i).cloneToRawat());
        }
        korban.setRawats(rawatList);
        return korban;
    }
}
