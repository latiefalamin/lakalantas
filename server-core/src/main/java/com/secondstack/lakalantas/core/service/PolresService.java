package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.Polres;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PolresService {
    
    public void save(Polres polres);
    public void save(Polres [] polreses);
    public void update(Polres polres);
    public void delete(Polres polres);
    public void deletes(Polres[] polress);
    public Polres find(String id);
    public List<Polres> find();
    public Long count();
    public String generateNextId();
}
