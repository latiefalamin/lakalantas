package com.secondstack.lakalantas.core.simple;

import com.secondstack.lakalantas.core.domain.Penyidik;
import com.secondstack.lakalantas.core.domain.Polres;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/27/12
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimplePenyidik {
    private String nrp;
    private String polresId;
    private String polresNama;
    private String nama;
    private String pangkat;

    public SimplePenyidik() {
    }

    public SimplePenyidik(String nrp, String polresId, String polresNama, String nama, String pangkat) {
        this.nrp = nrp;
        this.polresId = polresId;
        this.polresNama = polresNama;
        this.nama = nama;
        this.pangkat = pangkat;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getPolresId() {
        return polresId;
    }

    public void setPolresId(String polresId) {
        this.polresId = polresId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getPolresNama() {
        return polresNama;
    }

    public void setPolresNama(String polresNama) {
        this.polresNama = polresNama;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimplePenyidik)) return false;

        SimplePenyidik that = (SimplePenyidik) o;

        if (nama != null ? !nama.equals(that.nama) : that.nama != null) return false;
        if (nrp != null ? !nrp.equals(that.nrp) : that.nrp != null) return false;
        if (pangkat != null ? !pangkat.equals(that.pangkat) : that.pangkat != null) return false;
        if (polresId != null ? !polresId.equals(that.polresId) : that.polresId != null) return false;
        if (polresNama != null ? !polresNama.equals(that.polresNama) : that.polresNama != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nrp != null ? nrp.hashCode() : 0;
        result = 31 * result + (polresId != null ? polresId.hashCode() : 0);
        result = 31 * result + (polresNama != null ? polresNama.hashCode() : 0);
        result = 31 * result + (nama != null ? nama.hashCode() : 0);
        result = 31 * result + (pangkat != null ? pangkat.hashCode() : 0);
        return result;
    }

    public Penyidik cloneToPenyidik(){
        Polres polres = new Polres();
        polres.setId(polresId);
        polres.setNama(polresNama);

        Penyidik penyidik = new Penyidik();
        penyidik.setNama(nama);
        penyidik.setNrp(nrp);
        penyidik.setPangkat(pangkat);
        penyidik.setPolres(polres);

        return penyidik;
    }
}
