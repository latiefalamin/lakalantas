package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Asuransi;
import com.secondstack.lakalantas.core.domain.RumahSakit;
import com.secondstack.lakalantas.core.service.RumahSakitService;
import com.secondstack.lakalantas.core.util.NumberUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("rumahSakitService")
public class RumahSakitServiceImpl implements RumahSakitService {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void save(RumahSakit rumahSakit) {
        if (rumahSakit == null) throw new IllegalArgumentException("Cannot save null RumahSakit");
        if(rumahSakit.getId() == null || rumahSakit.getId().trim().isEmpty()){
            rumahSakit.setId(generateNextId());
        }
        sessionFactory.getCurrentSession().saveOrUpdate(rumahSakit);
    }

    @Override
    public void save(RumahSakit[] rumahSakites) {
        for (RumahSakit rumahSakit : rumahSakites)
            sessionFactory.getCurrentSession().saveOrUpdate(rumahSakit);
    }

    @Override
    public void delete(RumahSakit rumahSakit) {
        sessionFactory.getCurrentSession().delete(rumahSakit);
    }

    @Override
    public void deletes(RumahSakit[] rumahSakits) {
        for(RumahSakit rumahSakit:rumahSakits){
            delete(rumahSakit);
        }
    }

    @Override
    public RumahSakit find(String id) {
        return (RumahSakit) sessionFactory.getCurrentSession().get(RumahSakit.class, id);
    }

    @Override
    public List<RumahSakit> find() {
        return (List<RumahSakit>) sessionFactory.getCurrentSession().createQuery(
                "SELECT rumahSakit from RumahSakit rumahSakit order by rumahSakit.id")
                .list();
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(rumahSakit) from RumahSakit rumahSakit ")
                .uniqueResult();
    }


    @Override
    public String generateNextId() {
        String lastId = (String) sessionFactory.getCurrentSession()
                .createQuery("SELECT rs.id FROM RumahSakit rs ORDER BY rs.id")
                .setFirstResult((int) (count() - 1)).setMaxResults(1).uniqueResult();
        return lastId == null ? "01" : NumberUtils.toString(Integer.parseInt(lastId) + 1, 2);
    }
}
