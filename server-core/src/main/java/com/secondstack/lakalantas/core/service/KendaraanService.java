package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.Kendaraan;
import com.secondstack.lakalantas.core.simple.SimpleKendaraan;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface KendaraanService {
    
    public void save(Kendaraan kendaraan);
    public void save(SimpleKendaraan kendaraan);
    public void delete(Kendaraan kendaraan);
    public void deletes(Kendaraan[] kendaraans);
    public Kendaraan find(String id);
    public List<Kendaraan> find();
    public SimpleKendaraan findSimple(String id);
    public List<SimpleKendaraan> findSimple();
    public Long count();
}
