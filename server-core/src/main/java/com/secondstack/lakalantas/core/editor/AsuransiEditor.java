package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.Asuransi;
import com.secondstack.lakalantas.core.service.AsuransiService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class AsuransiEditor extends PropertyEditorSupport {

    private AsuransiService asuransiService;

    public AsuransiEditor(AsuransiService asuransiService) {
        this.asuransiService = asuransiService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Asuransi asuransi = asuransiService.find(text);

        if (asuransi != null) {
            setValue(asuransi);
        } else {
            throw new IllegalArgumentException("Asuransi with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Asuransi asuransi = (Asuransi) getValue();
            return asuransi.getId() + "";
        } else {
            return "";
        }
    }
}
