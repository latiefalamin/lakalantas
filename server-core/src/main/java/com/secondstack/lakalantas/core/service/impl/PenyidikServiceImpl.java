package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Penyidik;
import com.secondstack.lakalantas.core.service.PenyidikService;
import com.secondstack.lakalantas.core.simple.SimplePenyidik;
import com.secondstack.lakalantas.core.util.NumberUtils;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("penyidikService")
public class PenyidikServiceImpl implements PenyidikService {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void save(Penyidik penyidik) {
        if (penyidik == null) throw new IllegalArgumentException("Cannot save null Penyidik");
        if(penyidik.getNrp() == null || penyidik.getNrp().trim().isEmpty()){
            penyidik.setNrp(penyidik.getPolres().getId() + generateNextId(penyidik.getPolres().getId()));
        }
        sessionFactory.getCurrentSession().saveOrUpdate(penyidik);
    }

    @Override
    public void save(SimplePenyidik simplePenyidik) {
        if (simplePenyidik == null) throw new IllegalArgumentException("Cannot save null SimplePenyidik");
        save(simplePenyidik.cloneToPenyidik());
    }

    @Override
    public void delete(Penyidik penyidik) {
        sessionFactory.getCurrentSession().delete(penyidik);
    }

    @Override
    public void deletes(Penyidik[] penyidiks) {
        for(Penyidik penyidik:penyidiks){
            delete(penyidik);
        }
    }

    @Override
    public Penyidik find(String id) {
        return (Penyidik) sessionFactory.getCurrentSession().get(Penyidik.class, id);
    }

    @Override
    public List<Penyidik> find() {
        return (List<Penyidik>) sessionFactory.getCurrentSession().createQuery(
                "SELECT penyidik from Penyidik penyidik order by penyidik.id")
                .list();
    }

    @Override
    public SimplePenyidik findSimple(String id) {
        Penyidik penyidik = find(id);
        return penyidik == null ? null : penyidik.cloneToSimplePenyidik();
    }

    @Override
    public List<SimplePenyidik> findSimple() {
        return (List<SimplePenyidik>) sessionFactory.getCurrentSession().createQuery(
                "SELECT new com.secondstack.lakalantas.core.simple.SimplePenyidik(" +
                        "   penyidik.nrp," +
                        "   penyidik.polres.id, " +
                        "   penyidik.polres.nama, " +
                        "   penyidik.nama, " +
                        "   penyidik.pangkat" +
                        ")" +
                        "from " +
                        "   Penyidik penyidik " +
                        "order by penyidik.id")
                .list();
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(penyidik) from Penyidik penyidik ")
                .uniqueResult();
    }

    @Override
    public String generateNextId(String polresId) {
        String lastId = (String) sessionFactory.getCurrentSession()
                .createQuery("SELECT p.id FROM Penyidik p WHERE p.polres.id = :polresId ORDER BY p.id")
                .setParameter("polresId", polresId).setFirstResult((int) (count() - 1)).setMaxResults(1).uniqueResult();
        return lastId == null ? "01" : NumberUtils.toString(Integer.parseInt(lastId.substring(lastId.length() - 2)) + 1, 2);
    }
}
