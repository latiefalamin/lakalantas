package com.secondstack.lakalantas.core.domain.enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */
public enum KondisiKendaraan {
    TIDAK_RUSAK, RUSAK_RINGAN, RUSAK_PARAH;
}
