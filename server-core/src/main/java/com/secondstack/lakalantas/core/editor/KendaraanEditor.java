package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.Kendaraan;
import com.secondstack.lakalantas.core.service.KendaraanService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class KendaraanEditor extends PropertyEditorSupport {

    private KendaraanService kendaraanService;

    public KendaraanEditor(KendaraanService kendaraanService) {
        this.kendaraanService = kendaraanService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Kendaraan kendaraan = kendaraanService.find(text);

        if (kendaraan != null) {
            setValue(kendaraan);
        } else {
            throw new IllegalArgumentException("Kendaraan with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Kendaraan kendaraan = (Kendaraan) getValue();
            return kendaraan.getNopol() + "";
        } else {
            return "";
        }
    }
}
