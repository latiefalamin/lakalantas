package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Asuransi;
import com.secondstack.lakalantas.core.service.AsuransiService;
import com.secondstack.lakalantas.core.util.NumberUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("asuransiService")
public class AsuransiServiceImpl implements AsuransiService {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void save(Asuransi asuransi) {
        if (asuransi == null) throw new IllegalArgumentException("Cannot save null Asuransi");
        if(asuransi.getId() == null || asuransi.getId().trim().isEmpty()){
            asuransi.setId(generateNextId());
        }
        sessionFactory.getCurrentSession().saveOrUpdate(asuransi);
    }

    @Override
    public void save(Asuransi[] asuransies) {
        for (Asuransi asuransi : asuransies)
            sessionFactory.getCurrentSession().saveOrUpdate(asuransi);
    }

    @Override
    public void delete(Asuransi asuransi) {
        sessionFactory.getCurrentSession().delete(asuransi);
    }

    @Override
    public void deletes(Asuransi[] asuransis) {
        for(Asuransi asuransi:asuransis){
            delete(asuransi);
        }
    }

    @Override
    public Asuransi find(String id) {
        return (Asuransi) sessionFactory.getCurrentSession().get(Asuransi.class, id);
    }

    @Override
    public List<Asuransi> find() {
        return (List<Asuransi>) sessionFactory.getCurrentSession().createQuery(
                "SELECT asuransi from Asuransi asuransi order by asuransi.id")
                .list();
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(asuransi) from Asuransi asuransi ")
                .uniqueResult();
    }

    @Override
    public String generateNextId() {
        String lastId = (String) sessionFactory.getCurrentSession()
                .createQuery("SELECT a.id FROM Asuransi a ORDER BY a.id")
                .setFirstResult((int) (count() - 1)).setMaxResults(1).uniqueResult();
        return lastId == null ? "01" : NumberUtils.toString(Integer.parseInt(lastId) + 1, 2);
    }
}
