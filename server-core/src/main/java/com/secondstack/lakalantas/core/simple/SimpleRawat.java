package com.secondstack.lakalantas.core.simple;

import com.secondstack.lakalantas.core.domain.Rawat;
import com.secondstack.lakalantas.core.domain.RumahSakit;
import com.secondstack.lakalantas.core.util.CalendarUtils;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 4/20/12
 * Time: 7:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleRawat {
    private String id;
    private RumahSakit rumahSakit;
    private String waktuMasuk;
    private String waktuKeluar;
    private Float biaya;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RumahSakit getRumahSakit() {
        return rumahSakit;
    }

    public void setRumahSakit(RumahSakit rumahSakit) {
        this.rumahSakit = rumahSakit;
    }

    public String getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(String waktuMasuk) {
        this.waktuMasuk = waktuMasuk == null ? null : waktuMasuk.trim();
    }

    public String getWaktuKeluar() {
        return waktuKeluar;
    }

    public void setWaktuKeluar(String waktuKeluar) {
        this.waktuKeluar = waktuKeluar == null ? null : waktuKeluar.trim();
    }

    public Float getBiaya() {
        return biaya;
    }

    public void setBiaya(Float biaya) {
        this.biaya = biaya;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleRawat)) return false;

        SimpleRawat that = (SimpleRawat) o;

        if (biaya != null ? !biaya.equals(that.biaya) : that.biaya != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (rumahSakit != null ? !rumahSakit.equals(that.rumahSakit) : that.rumahSakit != null) return false;
        if (waktuKeluar != null ? !waktuKeluar.equals(that.waktuKeluar) : that.waktuKeluar != null) return false;
        if (waktuMasuk != null ? !waktuMasuk.equals(that.waktuMasuk) : that.waktuMasuk != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (rumahSakit != null ? rumahSakit.hashCode() : 0);
        result = 31 * result + (waktuMasuk != null ? waktuMasuk.hashCode() : 0);
        result = 31 * result + (waktuKeluar != null ? waktuKeluar.hashCode() : 0);
        result = 31 * result + (biaya != null ? biaya.hashCode() : 0);
        return result;
    }

    public Rawat cloneToRawat(){
        Rawat rawat = new Rawat();
        rawat.setBiaya(biaya);
        rawat.setId(id);
        rawat.setRumahSakit(rumahSakit);
        rawat.setWaktuKeluar(CalendarUtils.stringToTime(waktuKeluar));
        rawat.setWaktuMasuk(CalendarUtils.stringToTime(waktuMasuk));
        return rawat;
    }
}
