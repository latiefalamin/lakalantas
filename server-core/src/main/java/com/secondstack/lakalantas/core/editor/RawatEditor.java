package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.Rawat;
import com.secondstack.lakalantas.core.service.RawatService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class RawatEditor extends PropertyEditorSupport {

    private RawatService rawatService;

    public RawatEditor(RawatService rawatService) {
        this.rawatService = rawatService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Rawat rawat = rawatService.find(text);

        if (rawat != null) {
            setValue(rawat);
        } else {
            throw new IllegalArgumentException("Rawat with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Rawat rawat = (Rawat) getValue();
            return rawat.getId() + "";
        } else {
            return "";
        }
    }
}
