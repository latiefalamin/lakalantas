package com.secondstack.lakalantas.core.domain;

import com.secondstack.lakalantas.core.domain.enumeration.JenisLakalantas;
import com.secondstack.lakalantas.core.domain.enumeration.KondisiKendaraan;
import com.secondstack.lakalantas.core.simple.SimpleLakalantas;
import com.secondstack.lakalantas.core.util.CalendarUtils;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "lakalantas")
public class Lakalantas {

    //3 digit awal adalah id polres 12 digit akhir waktu kejadian YYYYMMddhhmm
    @Id
    @Column(name = "lakalantas_id", length = 15)
    private String id;

    @ManyToOne
    @JoinColumn(name = "nopol", nullable = false)
    private Kendaraan kendaraan;

    @ManyToOne
    @JoinColumn(name = "nrp", nullable = false)
    private Penyidik penyidik;

    @Column(name = "jenis_lakalantas", nullable = false)
    private JenisLakalantas jenisLakalantas;

    @Column(name = "kondisi_kendaraan", nullable = false)
    private KondisiKendaraan kondisiKendaraan;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "waktu_kejadian", nullable = false)
    private Calendar waktuKejadian;

    @Column(name = "tempat_kejadian", nullable = false)
    private String tempatKejadian;

    public Lakalantas() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Kendaraan getKendaraan() {
        return kendaraan;
    }

    public void setKendaraan(Kendaraan kendaraan) {
        this.kendaraan = kendaraan;
    }

    public Penyidik getPenyidik() {
        return penyidik;
    }

    public void setPenyidik(Penyidik penyidik) {
        this.penyidik = penyidik;
    }

    public JenisLakalantas getJenisLakalantas() {
        return jenisLakalantas;
    }

    public void setJenisLakalantas(JenisLakalantas jenisLakalantas) {
        this.jenisLakalantas = jenisLakalantas;
    }

    public KondisiKendaraan getKondisiKendaraan() {
        return kondisiKendaraan;
    }

    public void setKondisiKendaraan(KondisiKendaraan kondisiKendaraan) {
        this.kondisiKendaraan = kondisiKendaraan;
    }

    public Calendar getWaktuKejadian() {
        return waktuKejadian;
    }

    public void setWaktuKejadian(Calendar waktuKejadian) {
        this.waktuKejadian = waktuKejadian;
    }

    public String getTempatKejadian() {
        return tempatKejadian;
    }

    public void setTempatKejadian(String tempatKejadian) {
        this.tempatKejadian = tempatKejadian;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lakalantas)) return false;

        Lakalantas that = (Lakalantas) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (jenisLakalantas != that.jenisLakalantas) return false;
        if (kendaraan != null ? !kendaraan.equals(that.kendaraan) : that.kendaraan != null) return false;
        if (kondisiKendaraan != that.kondisiKendaraan) return false;
        if (penyidik != null ? !penyidik.equals(that.penyidik) : that.penyidik != null) return false;
        if (tempatKejadian != null ? !tempatKejadian.equals(that.tempatKejadian) : that.tempatKejadian != null)
            return false;
        if (waktuKejadian != null ? !waktuKejadian.equals(that.waktuKejadian) : that.waktuKejadian != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (kendaraan != null ? kendaraan.hashCode() : 0);
        result = 31 * result + (penyidik != null ? penyidik.hashCode() : 0);
        result = 31 * result + (jenisLakalantas != null ? jenisLakalantas.hashCode() : 0);
        result = 31 * result + (kondisiKendaraan != null ? kondisiKendaraan.hashCode() : 0);
        result = 31 * result + (waktuKejadian != null ? waktuKejadian.hashCode() : 0);
        result = 31 * result + (tempatKejadian != null ? tempatKejadian.hashCode() : 0);
        return result;
    }

    public SimpleLakalantas cloneToSimpleLakalantas(){
        SimpleLakalantas simpleLakalantas = new SimpleLakalantas();
        simpleLakalantas.setId(id);
        simpleLakalantas.setJenisLakalantas(jenisLakalantas);
        simpleLakalantas.setKondisiKendaraan(kondisiKendaraan);
        simpleLakalantas.setTempatKejadian(tempatKejadian);
        simpleLakalantas.setWaktuKejadian(CalendarUtils.timeToString(waktuKejadian));
        if(kendaraan != null){
            simpleLakalantas.setNopol(kendaraan.getNopol());
            if(kendaraan.getPolres() != null){
                simpleLakalantas.setPolresKendaraanId(kendaraan.getPolres().getId());
                simpleLakalantas.setPolresKendaraanNama(kendaraan.getPolres().getNama());
            }
        }
        if(penyidik != null){
            simpleLakalantas.setPenyidikNama(penyidik.getNama());
            simpleLakalantas.setPenyidikNrp(penyidik.getNrp());
            if(penyidik.getPolres() != null){
                simpleLakalantas.setPolresPenyidikId(penyidik.getPolres().getId());
                simpleLakalantas.setPolresPenyidikNama(penyidik.getPolres().getNama());
            }
        }
        return simpleLakalantas;
    }
}
