package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.Penyidik;
import com.secondstack.lakalantas.core.service.PenyidikService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class PenyidikEditor extends PropertyEditorSupport {

    private PenyidikService penyidikService;

    public PenyidikEditor(PenyidikService penyidikService) {
        this.penyidikService = penyidikService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Penyidik penyidik = penyidikService.find(text);

        if (penyidik != null) {
            setValue(penyidik);
        } else {
            throw new IllegalArgumentException("Penyidik with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Penyidik penyidik = (Penyidik) getValue();
            return penyidik.getNrp() + "";
        } else {
            return "";
        }
    }
}
