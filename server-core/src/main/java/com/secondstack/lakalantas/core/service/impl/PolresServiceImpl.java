package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;
import com.secondstack.lakalantas.core.util.NumberUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.transaction.TransactionManager;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("polresService")
public class PolresServiceImpl implements PolresService {

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public void save(Polres polres) {
        if (polres == null) throw new IllegalArgumentException("Cannot save null Polres");
        polres.setId(generateNextId());
        sessionFactory.getCurrentSession().save(polres);
    }

    @Override
    public void save(Polres[] polreses) {
        for (Polres polres : polreses)
            sessionFactory.getCurrentSession().saveOrUpdate(polres);
    }

    @Override
    public void update(Polres polres) {
        sessionFactory.getCurrentSession().update(polres);
    }

    @Override
    public void delete(Polres polres) {
        sessionFactory.getCurrentSession().delete(polres);
    }

    @Override
    public void deletes(Polres[] polress) {
        for (Polres polres : polress) {
            delete(polres);
        }
    }

    @Override
    public Polres find(String id) {
        return (Polres) sessionFactory.getCurrentSession().get(Polres.class, id);
    }

    @Override
    public List<Polres> find() {
        return (List<Polres>) sessionFactory.getCurrentSession().createQuery(
                "SELECT polres from Polres polres order by polres.id")
                .list();
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(polres) from Polres polres ")
                .uniqueResult();
    }

    @Override
    public String generateNextId() {
        String lastId = (String) sessionFactory.getCurrentSession()
                .createQuery("SELECT p.id FROM Polres p ORDER BY p.id")
                .setFirstResult((int) (count() - 1)).setMaxResults(1).uniqueResult();
        return lastId == null ? "001" : NumberUtils.toString(Integer.parseInt(lastId) + 1, 3);
    }
}
