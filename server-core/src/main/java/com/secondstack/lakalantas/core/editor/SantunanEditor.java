package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.Santunan;
import com.secondstack.lakalantas.core.service.SantunanService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class SantunanEditor extends PropertyEditorSupport {

    private SantunanService santunanService;

    public SantunanEditor(SantunanService santunanService) {
        this.santunanService = santunanService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Santunan santunan = santunanService.find(text);

        if (santunan != null) {
            setValue(santunan);
        } else {
            throw new IllegalArgumentException("Santunan with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Santunan santunan = (Santunan) getValue();
            return santunan.getId() + "";
        } else {
            return "";
        }
    }
}
