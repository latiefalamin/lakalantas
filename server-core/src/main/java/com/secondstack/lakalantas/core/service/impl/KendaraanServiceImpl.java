package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Kendaraan;
import com.secondstack.lakalantas.core.service.KendaraanService;
import com.secondstack.lakalantas.core.simple.SimpleKendaraan;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("kendaraanService")
public class KendaraanServiceImpl implements KendaraanService {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void save(Kendaraan kendaraan) {
        sessionFactory.getCurrentSession().saveOrUpdate(kendaraan);
    }

    @Override
    public void save(SimpleKendaraan kendaraan) {
        sessionFactory.getCurrentSession().saveOrUpdate(kendaraan.cloneToKendaraan());
    }

    @Override
    public void delete(Kendaraan kendaraan) {
        sessionFactory.getCurrentSession().delete(kendaraan);
    }

    @Override
    public void deletes(Kendaraan[] kendaraans) {
        for(Kendaraan kendaraan:kendaraans){
            delete(kendaraan);
        }
    }

    @Override
    public Kendaraan find(String id) {
        return (Kendaraan) sessionFactory.getCurrentSession().get(Kendaraan.class, id);
    }

    @Override
    public List<Kendaraan> find() {
        return (List<Kendaraan>) sessionFactory.getCurrentSession().createQuery(
                "SELECT kendaraan from Kendaraan kendaraan order by kendaraan.nopol")
                .list();
    }

    @Override
    public SimpleKendaraan findSimple(String id) {
        return find(id).cloneToSimpleKendaraan();
    }

    @Override
    public List<SimpleKendaraan> findSimple() {
        return  (List<SimpleKendaraan>) sessionFactory.getCurrentSession().createQuery(
                "SELECT new com.secondstack.lakalantas.core.simple.SimpleKendaraan(" +
                        "   kendaraan.id," +
                        "   kendaraan.polres.id, " +
                        "   kendaraan.polres.nama, " +
                        "   kendaraan.jenisKendaraan, " +
                        "   kendaraan.merk" +
                ") " +
                "from Kendaraan kendaraan " +
                "order by kendaraan.nopol ")
                .list();
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(kendaraan) from Kendaraan kendaraan ")
                .uniqueResult();
    }
}
