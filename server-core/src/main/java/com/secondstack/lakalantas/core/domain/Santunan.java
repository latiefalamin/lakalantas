package com.secondstack.lakalantas.core.domain;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:30 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "santunan")
public class Santunan {

    /**
     * 17 digit awal adalah id korban
     * 2 digit akhir adalah id asuransi
     */
    @Id
    @Column(name = "santunan_id", length = 19)
    private String id;
    
    @ManyToOne
    @JoinColumn(name = "asuransi_id", nullable = false)
    private Asuransi asuransi;
    
    @Column(name = "biaya_santunan", nullable = false)
    private Float biayaSantunan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Asuransi getAsuransi() {
        return asuransi;
    }

    public void setAsuransi(Asuransi asuransi) {
        this.asuransi = asuransi;
    }

    public Float getBiayaSantunan() {
        return biayaSantunan;
    }

    public void setBiayaSantunan(Float biayaSantunan) {
        this.biayaSantunan = biayaSantunan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Santunan)) return false;

        Santunan santunan = (Santunan) o;

        if (asuransi != null ? !asuransi.equals(santunan.asuransi) : santunan.asuransi != null) return false;
        if (biayaSantunan != null ? !biayaSantunan.equals(santunan.biayaSantunan) : santunan.biayaSantunan != null)
            return false;
        if (id != null ? !id.equals(santunan.id) : santunan.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (asuransi != null ? asuransi.hashCode() : 0);
        result = 31 * result + (biayaSantunan != null ? biayaSantunan.hashCode() : 0);
        return result;
    }
}
