package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.Lakalantas;
import com.secondstack.lakalantas.core.service.LakalantasService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class LakalantasEditor extends PropertyEditorSupport {

    private LakalantasService lakalantasService;

    public LakalantasEditor(LakalantasService lakalantasService) {
        this.lakalantasService = lakalantasService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Lakalantas lakalantas = lakalantasService.find(text);

        if (lakalantas != null) {
            setValue(lakalantas);
        } else {
            throw new IllegalArgumentException("Lakalantas with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Lakalantas lakalantas = (Lakalantas) getValue();
            return lakalantas.getId() + "";
        } else {
            return "";
        }
    }
}
