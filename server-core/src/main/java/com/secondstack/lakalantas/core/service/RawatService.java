package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.Rawat;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface RawatService {
    
    public void save(Rawat rawat);
    public void save(List<Rawat> rawats);
    public void delete(Rawat rawat);
    public void deletes(Rawat[] rawats);
    public Rawat find(String id);
    public List<Rawat> find();
    public Long count();
    public String generateNextId();
}
