package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Korban;
import com.secondstack.lakalantas.core.domain.Rawat;
import com.secondstack.lakalantas.core.domain.Santunan;
import com.secondstack.lakalantas.core.service.KorbanService;
import com.secondstack.lakalantas.core.service.RawatService;
import com.secondstack.lakalantas.core.service.SantunanService;
import com.secondstack.lakalantas.core.simple.SimpleKorban;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("korbanService")
public class KorbanServiceImpl implements KorbanService {
    
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private SantunanService santunanService;

    @Autowired
    private RawatService rawatService;

    @Override
    public void save(Korban korban) {
        if (korban == null) throw new IllegalArgumentException("Cannot save null Korban");

        sessionFactory.getCurrentSession().saveOrUpdate(korban);
    }

    @Override
    public void delete(Korban korban) {
        korban = find(korban.getId());
        Hibernate.initialize(korban.getSantunans());
        Hibernate.initialize(korban.getRawats());
        for(int i=0;korban.getSantunans() != null && i < korban.getSantunans().size();i++){
            santunanService.delete(korban.getSantunans().get(i));
        }

        for(int i=0;korban.getRawats() != null && i < korban.getRawats().size();i++){
            rawatService.delete(korban.getRawats().get(i));
        }

        sessionFactory.getCurrentSession().delete(korban);
    }

    @Override
    public void deletes(Korban[] korbans) {
        for(Korban korban:korbans){
            delete(korban);
        }
    }

    @Override
    public Korban find(String id) {
        Korban korban = (Korban) sessionFactory.getCurrentSession().get(Korban.class, id);
        Hibernate.initialize(korban.getRawats());
        Hibernate.initialize(korban.getSantunans());
        return korban;
    }

    @Override
    public List<Korban> find() {
        List<Korban> korbans = (List<Korban>) sessionFactory.getCurrentSession().createQuery(
                "SELECT korban from Korban korban order by korban.id")
                .list();
        for(Korban korban:korbans){
            Hibernate.initialize(korban.getRawats());
            Hibernate.initialize(korban.getSantunans());
        }
        return korbans;
    }

    @Override
    public List<SimpleKorban> findSimple() {
        List<Korban> korbans = find();
        if(korbans == null) return null;
        List<SimpleKorban> simpleKorbans = new ArrayList<SimpleKorban>();
        for(Korban korban:korbans){
            simpleKorbans.add(korban.cloneToSimpleKorban());
        }
        return simpleKorbans;
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(korban) from Korban korban ")
                .uniqueResult();
    }

    @Override
    public String generateNextId() {
        return new GregorianCalendar().getTimeInMillis() + "";
    }
}
