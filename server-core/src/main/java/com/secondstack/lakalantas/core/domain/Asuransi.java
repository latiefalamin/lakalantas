package com.secondstack.lakalantas.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:31 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "asuransi")
public class Asuransi {

    @Id
    @Column(name = "asuransi_id", length = 2)
    private String id;

    @Column(name = "nama", nullable = false, length = 50)
    private String nama;

    @Column(name = "alamat", nullable = false)
    private String alamat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Asuransi)) return false;

        Asuransi asuransi = (Asuransi) o;

        if (alamat != null ? !alamat.equals(asuransi.alamat) : asuransi.alamat != null) return false;
        if (id != null ? !id.equals(asuransi.id) : asuransi.id != null) return false;
        if (nama != null ? !nama.equals(asuransi.nama) : asuransi.nama != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nama != null ? nama.hashCode() : 0);
        result = 31 * result + (alamat != null ? alamat.hashCode() : 0);
        return result;
    }
}
