package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.Penyidik;
import com.secondstack.lakalantas.core.simple.SimplePenyidik;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PenyidikService {
    
    public void save(Penyidik penyidik);
    public void save(SimplePenyidik simplePenyidik);
    public void delete(Penyidik penyidik);
    public void deletes(Penyidik[] penyidiks);
    public Penyidik find(String id);
    public List<Penyidik> find();
    public SimplePenyidik findSimple(String id);
    public List<SimplePenyidik> findSimple();
    public Long count();
    public String generateNextId(String polresId);
}
