package com.secondstack.lakalantas.core.domain;

import com.secondstack.lakalantas.core.domain.enumeration.JenisKendaraan;
import com.secondstack.lakalantas.core.simple.SimpleKendaraan;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "kendaraan")
public class Kendaraan {

    @Id
    @Column(name = "nopol", length = 10)
    private String nopol;

    @ManyToOne
    @JoinColumn(name = "polres_id", nullable = false)
    private Polres polres;

    @Column(name = "jenis_kendaraan", nullable = false)
    private JenisKendaraan jenisKendaraan;

    @Column(name = "merk", nullable = false, length = 20)
    private String merk;

    public String getNopol() {
        return nopol;
    }

    public void setNopol(String nopol) {
        this.nopol = nopol;
    }

    public Polres getPolres() {
        return polres;
    }

    public void setPolres(Polres polres) {
        this.polres = polres;
    }

    public JenisKendaraan getJenisKendaraan() {
        return jenisKendaraan;
    }

    public void setJenisKendaraan(JenisKendaraan jenisKendaraan) {
        this.jenisKendaraan = jenisKendaraan;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Kendaraan)) return false;

        Kendaraan kendaraan = (Kendaraan) o;

        if (jenisKendaraan != kendaraan.jenisKendaraan) return false;
        if (merk != null ? !merk.equals(kendaraan.merk) : kendaraan.merk != null) return false;
        if (nopol != null ? !nopol.equals(kendaraan.nopol) : kendaraan.nopol != null) return false;
        if (polres != null ? !polres.equals(kendaraan.polres) : kendaraan.polres != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nopol != null ? nopol.hashCode() : 0;
        result = 31 * result + (polres != null ? polres.hashCode() : 0);
        result = 31 * result + (jenisKendaraan != null ? jenisKendaraan.hashCode() : 0);
        result = 31 * result + (merk != null ? merk.hashCode() : 0);
        return result;
    }

    public SimpleKendaraan cloneToSimpleKendaraan(){
        SimpleKendaraan simpleKendaraan = new SimpleKendaraan();
        simpleKendaraan.setJenisKendaraan(jenisKendaraan);
        simpleKendaraan.setMerk(merk);
        simpleKendaraan.setNopol(nopol);
        simpleKendaraan.setPolresId(polres.getId());
        simpleKendaraan.setPolresNama(polres.getNama());
        return simpleKendaraan;
    }
}
