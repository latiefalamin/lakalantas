package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.Korban;
import com.secondstack.lakalantas.core.simple.SimpleKorban;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface KorbanService {
    
    public void save(Korban korban);
    public void delete(Korban korban);
    public void deletes(Korban[] korbans);
    public Korban find(String id);
    public List<Korban> find();
    public List<SimpleKorban> findSimple();
    public Long count();
    public String generateNextId();
}
