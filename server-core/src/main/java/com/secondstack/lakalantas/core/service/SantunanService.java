package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.Santunan;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SantunanService {
    
    public void save(Santunan santunan);
    public void save(List<Santunan> santunans);
    public void delete(Santunan santunan);
    public void deletes(Santunan[] santunans);
    public Santunan find(String id);
    public List<Santunan> find();
    public Long count();
    public String generateNextId();
}
