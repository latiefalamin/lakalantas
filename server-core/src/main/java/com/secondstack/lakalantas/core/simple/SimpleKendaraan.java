package com.secondstack.lakalantas.core.simple;

import com.secondstack.lakalantas.core.domain.Kendaraan;
import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.domain.enumeration.JenisKendaraan;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/27/12
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleKendaraan {
    private String nopol;
    private String polresId;
    private String polresNama;
    private JenisKendaraan jenisKendaraan;
    private String merk;

    public SimpleKendaraan() {
    }

    public SimpleKendaraan(String nopol, String polresId, String polresNama, JenisKendaraan jenisKendaraan, String merk) {
        this.nopol = nopol;
        this.polresId = polresId;
        this.polresNama = polresNama;
        this.jenisKendaraan = jenisKendaraan;
        this.merk = merk;
    }

    public String getNopol() {
        return nopol;
    }

    public void setNopol(String nopol) {
        this.nopol = nopol;
    }

    public String getPolresId() {
        return polresId;
    }

    public void setPolresId(String polresId) {
        this.polresId = polresId;
    }

    public String getPolresNama() {
        return polresNama;
    }

    public void setPolresNama(String polresNama) {
        this.polresNama = polresNama;
    }

    public JenisKendaraan getJenisKendaraan() {
        return jenisKendaraan;
    }

    public void setJenisKendaraan(JenisKendaraan jenisKendaraan) {
        this.jenisKendaraan = jenisKendaraan;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleKendaraan)) return false;

        SimpleKendaraan that = (SimpleKendaraan) o;

        if (jenisKendaraan != that.jenisKendaraan) return false;
        if (merk != null ? !merk.equals(that.merk) : that.merk != null) return false;
        if (nopol != null ? !nopol.equals(that.nopol) : that.nopol != null) return false;
        if (polresId != null ? !polresId.equals(that.polresId) : that.polresId != null) return false;
        if (polresNama != null ? !polresNama.equals(that.polresNama) : that.polresNama != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nopol != null ? nopol.hashCode() : 0;
        result = 31 * result + (polresId != null ? polresId.hashCode() : 0);
        result = 31 * result + (polresNama != null ? polresNama.hashCode() : 0);
        result = 31 * result + (jenisKendaraan != null ? jenisKendaraan.hashCode() : 0);
        result = 31 * result + (merk != null ? merk.hashCode() : 0);
        return result;
    }

    public Kendaraan cloneToKendaraan(){
        Polres polres = new Polres();
        polres.setId(polresId);
        polres.setNama(polresNama);

        Kendaraan kendaraan = new Kendaraan();
        kendaraan.setJenisKendaraan(jenisKendaraan);
        kendaraan.setNopol(nopol);
        kendaraan.setMerk(merk);
        kendaraan.setPolres(polres);

        return kendaraan;
    }
}
