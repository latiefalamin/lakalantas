package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.Polres;
import com.secondstack.lakalantas.core.service.PolresService;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class PolresEditor extends PropertyEditorSupport {

    private PolresService polresService;

    public PolresEditor(PolresService polresService) {
        this.polresService = polresService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Polres polres = polresService.find(text);

        if (polres != null) {
            setValue(polres);
        } else {
            throw new IllegalArgumentException("Polres with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Polres polres = (Polres) getValue();
            return polres.getId() + "";
        } else {
            return "";
        }
    }
}
