package com.secondstack.lakalantas.core.editor;

import com.secondstack.lakalantas.core.domain.Korban;
import com.secondstack.lakalantas.core.service.KorbanService;
import org.hibernate.Hibernate;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class KorbanEditor extends PropertyEditorSupport {

    private KorbanService korbanService;

    public KorbanEditor(KorbanService korbanService) {
        this.korbanService = korbanService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Korban korban = korbanService.find(text);

        if (korban != null) {
            setValue(korban);
        } else {
            throw new IllegalArgumentException("Korban with id " + text + " not Found!");
        }
    }

    @Override
    public String getAsText() {
        if (getValue() != null) {
            Korban korban = (Korban) getValue();
            return korban.getId() + "";
        } else {
            return "";
        }
    }
}
