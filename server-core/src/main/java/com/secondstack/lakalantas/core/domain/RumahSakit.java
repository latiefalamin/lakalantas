package com.secondstack.lakalantas.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Latief
 * Date: 3/4/12
 * Time: 9:32 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "rumah_sakit")
public class RumahSakit {

    @Id
    @Column(name = "rumah_sakit_id", length = 2)
    private String id;

    @Column(name = "nama", nullable = false, length = 50)
    private String nama;

    @Column(name = "alamat", nullable = false)
    private String alamat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RumahSakit)) return false;

        RumahSakit that = (RumahSakit) o;

        if (alamat != null ? !alamat.equals(that.alamat) : that.alamat != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (nama != null ? !nama.equals(that.nama) : that.nama != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nama != null ? nama.hashCode() : 0);
        result = 31 * result + (alamat != null ? alamat.hashCode() : 0);
        return result;
    }
}
