package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Lakalantas;
import com.secondstack.lakalantas.core.service.LakalantasService;
import com.secondstack.lakalantas.core.simple.SimpleLakalantas;
import com.secondstack.lakalantas.core.util.NumberUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("lakalantasService")
public class LakalantasServiceImpl implements LakalantasService {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Lakalantas lakalantas) {
        if (lakalantas == null) throw new IllegalArgumentException("Cannot save null Lakalantas");
        if (lakalantas.getId() == null || lakalantas.getId().trim().isEmpty()) {
            lakalantas.setId(generateNextId(lakalantas.getPenyidik().getPolres().getId(), lakalantas.getWaktuKejadian()));
        }
        sessionFactory.getCurrentSession().saveOrUpdate(lakalantas);
    }

    @Override
    public void save(SimpleLakalantas simpleLakalantas) {
        if (simpleLakalantas == null) throw new IllegalArgumentException("Cannot save null Lakalantas");
        save(simpleLakalantas.cloneToLakalantas());
    }

    @Override
    public void delete(Lakalantas lakalantas) {
        sessionFactory.getCurrentSession().delete(lakalantas);
    }

    @Override
    public void deletes(Lakalantas[] lakalantass) {
        for (Lakalantas lakalantas : lakalantass) {
            delete(lakalantas);
        }
    }

    @Override
    public Lakalantas find(String id) {
        return (Lakalantas) sessionFactory.getCurrentSession().get(Lakalantas.class, id);
    }

    @Override
    public SimpleLakalantas findSimple(String id) {
        Lakalantas lakalantas = find(id);
        return lakalantas == null ? null : lakalantas.cloneToSimpleLakalantas(); 
    }

    @Override
    public List<Lakalantas> find() {
        return (List<Lakalantas>) sessionFactory.getCurrentSession().createQuery(
                "SELECT lakalantas from Lakalantas lakalantas order by lakalantas.waktuKejadian desc ")
                .list();
    }

    @Override
    public List<SimpleLakalantas> findSimple() {
        List<Lakalantas> lakalantases = find();
        if(lakalantases == null) return null;
        List<SimpleLakalantas> simpleLakalantases = new ArrayList<SimpleLakalantas>();
        for(Lakalantas lakalantas:lakalantases){
            simpleLakalantases.add(lakalantas.cloneToSimpleLakalantas());
        }
        return simpleLakalantases;
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(lakalantas) from Lakalantas lakalantas ")
                .uniqueResult();
    }

    @Override
    public String generateNextId(String polresId, Calendar waktuKejadian) {
        Calendar cal = waktuKejadian == null ? new GregorianCalendar() : waktuKejadian;
        return polresId + cal.get(Calendar.YEAR) + cal.get(Calendar.MONTH) +
                cal.get(Calendar.DATE) + cal.get(Calendar.HOUR) + cal.get(Calendar.MINUTE);
    }
}
