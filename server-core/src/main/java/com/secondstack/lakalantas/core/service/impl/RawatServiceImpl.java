package com.secondstack.lakalantas.core.service.impl;

import com.secondstack.lakalantas.core.domain.Rawat;
import com.secondstack.lakalantas.core.service.RawatService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
@Transactional
//@Service("rawatService")
public class RawatServiceImpl implements RawatService {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void save(Rawat rawat) {
        if(rawat == null) throw new IllegalArgumentException("Cannot save null value of" + Rawat.class);
        rawat.setId(rawat.getId() == null ? generateNextId() : rawat.getId().trim());
        sessionFactory.getCurrentSession().saveOrUpdate(rawat);
    }

    @Override
    public void save(List<Rawat> rawats) {
        if(rawats == null) return;
        for(Rawat rawat:rawats){
            save(rawat);
        }
    }

    @Override
    public void delete(Rawat rawat) {
        sessionFactory.getCurrentSession().delete(rawat);
    }

    @Override
    public void deletes(Rawat[] rawats) {
        if(rawats == null) return;
        for(Rawat rawat:rawats){
            delete(rawat);
        }
    }

    @Override
    public Rawat find(String id) {
        return (Rawat) sessionFactory.getCurrentSession().get(Rawat.class, id);
    }

    @Override
    public List<Rawat> find() {
        return (List<Rawat>) sessionFactory.getCurrentSession().createQuery(
                "SELECT rawat from Rawat rawat order by rawat.id")
                .list();
    }

    @Override
    public Long count() {
        return (Long) sessionFactory.getCurrentSession().createQuery(
                "SELECT count(rawat) from Rawat rawat ")
                .uniqueResult();
    }

    @Override
    public String generateNextId() {
        return new GregorianCalendar().getTimeInMillis() + "";
    }
}
