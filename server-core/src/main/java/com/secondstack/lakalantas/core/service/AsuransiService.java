package com.secondstack.lakalantas.core.service;

import com.secondstack.lakalantas.core.domain.Asuransi;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: latief
 * Date: 3/18/12
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AsuransiService {
    
    public void save(Asuransi asuransi);
    public void save(Asuransi[]  asuransies);
    public void delete(Asuransi asuransi);
    public void deletes(Asuransi [] asuransis);
    public Asuransi find(String id);
    public List<Asuransi> find();
    public Long count();
    public String generateNextId();
}
